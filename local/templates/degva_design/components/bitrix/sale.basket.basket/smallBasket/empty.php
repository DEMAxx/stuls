<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
?>
<?
$APPLICATION->SetTitle("Ваша корзина пуста");
?>
<div class="center1200 center-block">
	<div class="info-page-wrapper">
		<div class="info-page-image">
			<img class="img" src="/images/svg/sad.svg" alt="">
		</div>
		<h1 class="h1"><?=Loc::getMessage("SBB_EMPTY_BASKET_TITLE")?></h1>
		<?
		if (!empty($arParams['EMPTY_BASKET_HINT_PATH']))
		{
			?>
			<div class="bx-sbb-empty-cart-desc">
				<?=Loc::getMessage(
					'SBB_EMPTY_BASKET_HINT',
					[
						'#A1#' => '<a class="button" href="'.$arParams['EMPTY_BASKET_HINT_PATH'].'">',
						'#A2#' => '</a>'
					]
				)?>
			</div>
			<?
		}
		?>
	</div>
</div>