<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>		
	<div class="menu bottom">
		<div class="clear"></div>
		<div class="title">Компания</div>
		<?foreach ($arResult as $key => $arItem):?>
			<div class="item_wrapper">
				<a class="item" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				<?if($arItem["ADDITIONAL_LINKS"]){?>
					<div class="sub_menu">
						<?foreach($arItem["ADDITIONAL_LINKS"] as $sub){?>
							<a  class="item" href="<?=$sub[1]?>"><?=$sub[0]?></a><br/>
						<?}?>
					</div>
				<?}?>
			</div>
		<?endforeach;?>
	</div>
<?endif?>
