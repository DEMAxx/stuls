<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();
if (!empty($arResult)):?>
	<?foreach ($arResult as $arItem):?>
		<div class="item_wrapper">
			<?if($arItem["LINK"]==$dir){?>
				<span class="selected"><?=$arItem["TEXT"]?></span>
			<?}else{?>
				<a class="item" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
			<?}?>
			<?if($arItem["ADDITIONAL_LINKS"]){?>
				<div class="sub_menu">
					<?foreach($arItem["ADDITIONAL_LINKS"] as $sub){?>
						<a  class="item" href="<?=$sub[1]?>"><?=$sub[0]?></a><br/>
					<?}?>
				</div>
			<?}?>
		</div>
    <?endforeach;?>
<?endif?>