<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(CModule::IncludeModule("iblock")){ //подключаем модуль инфоблок для работы с классом CIBlockSection
    $arResult["MENU"] = $arResult;
    $uf_arresult = CIBlockSection::GetList(Array(), Array("IBLOCK_ID" => 2), false, Array());
    while($uf_value = $uf_arresult->GetNext()){
        $arResult["SECTIONS"][$uf_value["ID"]]["LINK"] = $uf_value["SECTION_PAGE_URL"];
        $arResult["SECTIONS"][$uf_value["ID"]]["TEXT"] = $uf_value["NAME"];
    }
}