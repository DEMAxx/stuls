<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="menu-mobile">
<?
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();
if (!empty($arResult)):?>
	<section class="white section">
		<div class="close-wrapper"><div class="close"></div></div>
		<h3 class="h3">Каталог</h3>		
		<?foreach ($arResult["SECTIONS"] as $arItem):?>
			<div class="item_wrapper">
				<?if($arItem["LINK"]==$dir){?>
					<span class="item selected"><?=$arItem["TEXT"]?></span>
				<?}else{?>
					<a class="item" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				<?}?>
			</div>
		<?endforeach;?>
	</section>
	<section class="brown section">
		<?foreach ($arResult["MENU"] as $arItem):?>
			<div class="item_wrapper">
				<?if($arItem["LINK"]==$dir){?>
					<span class="item selected"><?=$arItem["TEXT"]?></span>
				<?}else{?>
					<a class="item" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				<?}?>
				<?if($arItem["ADDITIONAL_LINKS"]){?>
					<div class="sub_menu">
						<?foreach($arItem["ADDITIONAL_LINKS"] as $sub){?>
							<a class="item" href="<?=$sub[1]?>"><?=$sub[0]?></a><br/>
						<?}?>
					</div>
				<?}?>
			</div>
		<?endforeach;?>
		<div class="phone">			
			<a href="tel:+78007079724">8 800 707 97 24</a><br/>
			<a href="tel:+74912559724">8 4912 55 97 24</a>
			<div class="title">Телефон магазина</div>
		</div>
		<div class="mail">			
			<a href="mailto:stulsrzn@stuls.ru">stulsrzn@stuls.ru</a>
			<div class="title">Администратор Рязань</div>
		</div>
	</section>
<?endif?>
</div>
<?//pre($arResult);?>