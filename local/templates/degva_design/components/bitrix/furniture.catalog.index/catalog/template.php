<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<div class="share-block center1200">
	<?if (is_array($arResult['ITEMS']) && count($arResult['ITEMS']) > 0){?>
		<div class="product-list">
			<?foreach ($arResult['ITEMS'] as $arItem){?>
				<div class="product">
					<div class="product-overlay"></div>
					<a class="product-desc" <?if($arItem['PICTURE']['SRC']):?> style="background-image: url(<?=$arItem['PICTURE']['SRC']?>)"<?endif;?> href="<?=$arItem['DETAIL_URL']?>">
						<b><?=$arItem['NAME']?></b>
						<p><?=$arItem['DESCRIPTION']?></p>
					</a>
				</div>
			<?}?>
		</div>
	<?}?>
</div>
<?//pre($arResult['ITEMS']);?>