<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>
<div class="object center1200">
	<?if($arResult["PROPERTIES"]["OBJECT_VIDEO"]["VALUE"]){?>
		<iframe class="object_video" width="1200" height="600" src="https://www.youtube.com/embed/<?=$arResult["PROPERTIES"]["OBJECT_VIDEO"]["VALUE"]?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	
	<?}else{?>
		<div class="object_photo"><img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" title="<?=$arResult["NAME"]?>" alt="<?=$arResult["NAME"]?>" class="img"/></div>
	<?}?>
	<div class="main text"><?=$arResult["DETAIL_TEXT"]?></div>
	<?if(!empty($arResult["PROPERTIES"]["GALLERY"]["VALUE"])){?>
		<h5><?=$arResult["PROPERTIES"]["GALLERY_NAME"]["VALUE"]?></h5>
		<div class="pictures">
			<?foreach($arResult["PROPERTIES"]["GALLERY"]["VALUE"] as $key=>$picture){?>
				<img src="<?=CFile::GetPath($picture)?>" alt="<?=$arResult["NAME"]?>_<?=$key?>" class="picture"/>
			<?}?>
		</div>
	<?}?>
	<?if($arResult["PROPERTIES"]["REVIEW_VIDEO"]["VALUE"]){?>
		<iframe class="review_video" width="800" height="600" src="https://www.youtube.com/embed/<?=$arResult["PROPERTIES"]["REVIEW_VIDEO"]["VALUE"]?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	
		<div class="review_text"><?=$arResult["PROPERTIES"]["REVIEW_TEXT"]["VALUE"]?></div>
	<?}?>
<?//pre($arResult);?>
</div>