<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>
<div class="direction-page">
    <h1 style="background-image: url(<?=CFile::GetPath($arResult["PROPERTIES"]["TITLE_PICTURE"]["VALUE"]);?>);"><?=$arResult["NAME"]?></h1>
    <div class="full-center1200">
        <div class="ib2 img-detail" style="background-image: url('<?=$arResult["DETAIL_PICTURE"]["SRC"]?>');"></div><!--
    --><div class="ib2 text-detail"><?=$arResult["~PREVIEW_TEXT"]?></div>
        <div class="detail-info center1200"><?=$arResult["~DETAIL_TEXT"]?></div>
        <div class="under-detail-picture" style="background-image: url('<?=CFile::GetPath($arResult["PROPERTIES"]["UNDER_DETAIL"]["VALUE"]);?>');">
            <div class="text-inside"><?=$arResult["PROPERTIES"]["TEXT_IN_PICTURE"]["VALUE"];?></div>
        </div>
        <?if(!empty($arResult["PROPERTIES"]["GALLERY_UNDER_PICTURE"]["VALUE"])){?>
            <div class="flex-pictures">
                <?foreach($arResult["PROPERTIES"]["GALLERY_UNDER_PICTURE"]["VALUE"] as $key=>$picture){?>
                    <div class="flex-picture" style="background-image: url('<?=CFile::GetPath($picture);?>'); flex:<?=$arResult["PROPERTIES"]["GALLERY_UNDER_PICTURE"]["DESCRIPTION"][$key]?>"></div>
                <?}?>
            </div>
        <?}?>
    </div>
    <?$this->SetViewTarget("SOLUTIONS");?>
         <?if(!empty($arResult["SOLUTIONS_LIST"])){?>
            <div class="center1200 solutions">
                <h2 class="h2"><?=$arResult["SECTION_DESCRIPTION"]?></h2>
                <?foreach($arResult["SOLUTIONS_LIST"] as $solution){?>
                    <div class="solution ib2<?if($solution["PROPERTY_SVG_NAME_VALUE"]){?> with-svg<?}?>">
                        <?if($solution["PREVIEW_PICTURE"]){?>
                            <img class="img" src="<?=CFile::GetPath($solution["PREVIEW_PICTURE"])?>" title="<?=$solution["NAME"]?>" alt="<?=$solution["NAME"]?>">
                        <?}?>
                        <?if($solution["PROPERTY_SVG_NAME_VALUE"]){?>
                            <img class="svg" src="/images/svg/<?=$solution["PROPERTY_SVG_NAME_VALUE"]?>.svg" title="<?=$solution["NAME"]?>" alt="<?=$solution["NAME"]?>">
                        <?}?>
                        <h5><?=$solution["NAME"]?></h5>
                        <p><?=$solution["~PREVIEW_TEXT"]?></p>
                    </div>
                <?}?>
                <?if($arResult["SECTION_DETAIL_PICTURE"]){?>
                    <div class="under-detail-picture right-section" style="background-image: url('<?=CFile::GetPath($arResult["SECTION_DETAIL_PICTURE"]);?>');">
                        <div class="text-inside"><?=$arResult["TEXT_IN_PICTURE"];?></div>
                    </div>
                    <div class="text-under"><?=$arResult["UF_UNDER_PICTURE"];?></div>
                <?}?>
            </div> 
         <?}?>
        </div><!--end dir-->
    <?$this->EndViewTarget();?> 
    <?$cp = $this->__component;
    $cp->arResult['STYLE'] = $arResult["PROPERTIES"]["STYLE"]["VALUE"];
    $cp->SetResultCacheKeys(array('STYLE'));
    $arResult['STYLE'] = $cp->arResult['STYLE'];?>
<?//pre($arResult);?>

