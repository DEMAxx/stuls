<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_PICTURE", "PREVIEW_TEXT", "PROPERTY_SVG_NAME");
$arFilter = Array("IBLOCK_ID"=>IntVal($arResult["PROPERTIES"]["LINKS_TO_SOLUTIONS"]["LINK_IBLOCK_ID"]), "SECTION_ID"=> $arResult["PROPERTIES"]["LINKS_TO_SOLUTIONS"]["VALUE"], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNext())
{
	$arResult["SOLUTIONS_LIST"][] = $ob;
}
$arFilter2 = Array('IBLOCK_ID'=>IntVal($arResult["PROPERTIES"]["LINKS_TO_SOLUTIONS"]["LINK_IBLOCK_ID"]), 'ID'=>$arResult["PROPERTIES"]["LINKS_TO_SOLUTIONS"]["VALUE"]);
$arSelect2 = Array("ID", "PICTURE","DESCRIPTION","DETAIL_PICTURE","UF_*");
$res2 = CIBlockSection::GetList(Array("SORT"=>"ASC"),$arFilter2,false,$arSelect2);
if($ar_res = $res2->GetNext()){
	$arResult["SECTION_DESCRIPTION"] = $ar_res["DESCRIPTION"];
	$arResult["SECTION_PICTURE"] = $ar_res["PICTURE"];
	$arResult["SECTION_DETAIL_PICTURE"] = $ar_res["DETAIL_PICTURE"];
	$arResult["TEXT_IN_PICTURE"] = $ar_res["UF_IN_PICTURE"];
	$arResult["UF_UNDER_PICTURE"] = $ar_res["UF_UNDER_PICTURE"];
}
?>