<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>
<div class="center1200">
    <h3 class="h2"><?=$arResult["NAME"]?></h3>
    <div class="office-img" style="background-image: url('<?=$arResult["DETAIL_PICTURE"]["SRC"]?>');">
        <div class="description">
            <?if($arResult["PROPERTIES"]["ADRESS"]["VALUE"]){?>
                <div class="text adress"><?=$arResult["PROPERTIES"]["ADRESS"]["VALUE"]?></div>
            <?}?>
            <?if($arResult["PROPERTIES"]["PHONE"]["VALUE"]){?>
                <div class="text phone">
                    <?foreach($arResult["PROPERTIES"]["PHONE"]["VALUE"] as $value){?>
                        <p><?=$value?></p>
                    <?}?>
                </div>
            <?}?>
            <?if($arResult["PROPERTIES"]["MAIL"]["VALUE"]){?>
                <div class="text mail"><?=$arResult["PROPERTIES"]["MAIL"]["VALUE"]?></div>
            <?}?>
            <?if($arResult["PROPERTIES"]["TIME"]["VALUE"]){?>
                <div class="text time"><?=$arResult["PROPERTIES"]["TIME"]["VALUE"]?></div>
            <?}?>
        </div>
    </div>
</div>