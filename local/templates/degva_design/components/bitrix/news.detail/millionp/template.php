<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>
<?//pre($arResult);?>
<?if($arParams["FROM_AJAX"]!="Y"){?>
<div id="slide2" class="center1200 million-description slide2 slide">
	<h3 class="h2"><?=$arResult["IBLOCK"]["NAME"]?></h3>
	<div class="changer">
	<?}?>
		<div class="selectors">
		<?foreach($arResult["LINKS"] as $link){?>
			<?if($arResult["ID"]==$link["ID"]){?>
				<div class="item active"><?=$link["NAME"]?></div>
			<?}else{?>
				<a href="/team/" <?/*data-link="<?=$link["ID"]?>" data-iblock="<?=$arResult["IBLOCK_ID"]?>"*/?>class="item change"><?=$link["NAME"]?></a>
			<?}?>
		<?}?>
		</div>
		<div class="ib2">
			<?if($arResult["PROPERTIES"]["VIDEO_Y"]["VALUE"]){?>
				<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$arResult["PROPERTIES"]["VIDEO_Y"]["VALUE"]?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			<?}?>
		</div><!--
		--><div class="ib2 text"><?=$arResult["~PREVIEW_TEXT"]?></div>
		<div class="advantages">
			<?foreach($arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"] as $key=>$val){?>
				<div class="ib3">
					<h4><?=$val?></h4>
					<p><?=$arResult["PROPERTIES"]["ADVANTAGES"]["DESCRIPTION"][$key]?></p>
				</div>
			<?}?>
		</div>
		<div class="advantages left-picture">
			<?foreach($arResult["PROPERTIES"]["TEXT_ADVANTAGES"]["VALUE"] as $key=>$val){?>
				<div class="ib2">
					<h5><?=$arResult["PROPERTIES"]["TEXT_ADVANTAGES"]["DESCRIPTION"][$key]?></h5>
					<p><?=$val?></p>
				</div>
			<?}?>
		</div>
		<div class="slogan triangle">
			<?=$arResult["IBLOCK"]["~DESCRIPTION"]?>
			<div class="triangle-min"><div class="figure"></div><div class="car left"></div><div class="car right"></div></div>
		</div>
<?if($arParams["FROM_AJAX"]!="Y"){?>
	</div>
</div>
<?}?>