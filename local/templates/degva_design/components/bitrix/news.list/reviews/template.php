<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>
<div class="reviews-block center1200">
	<h3 class="h1">Отзывы наших покупателей</h3>
	<div class="gallery-reviews">
		<?foreach($arResult["ITEMS"] as $key=>$item){?>
			<?
            //\Stuls\Helper::pre($item);
			$estimation = round(($item["PROPERTIES"]["SHOP_ESTIMATION"]["VALUE_XML_ID"]+$item["PROPERTIES"]["STAFF_ESTIMATION"]["VALUE_XML_ID"]+$item["PROPERTIES"]["DELIVERY_ESTIMATION"]["VALUE_XML_ID"])/3);
			?>
			<div class="gal-item">
				<div class="date"><?if(strlen($item["DISPLAY_ACTIVE_FROM"])>0){?><?=$item["DISPLAY_ACTIVE_FROM"]?><?}else{?><?=$item["PROPERTIES"]["DATE"]["VALUE"]?><?}?></div>
				<h5><?=$item["PROPERTIES"]["NAME"]["VALUE"]?></h5>
				<div class="estimation">
					<?for ($i=0; $i < 5; $i++) {?>
						<div class="star<?if($i<$estimation){echo " active";}?>"><svg viewbox="0 0 475.075 475.075"><use xlink:href="#star"/></svg></div>
					<?}?>	
				</div>
				<?if($item["PROPERTIES"]["REVIEW"]["VALUE"]){
				    ?>
					<p class="text"><?if(strlen($item["PROPERTIES"]["REVIEW"]["VALUE"])>70){
						$show_part = mb_substr($item["PROPERTIES"]["REVIEW"]["~VALUE"], 0, 70);
						$hidden_part = mb_substr($item["PROPERTIES"]["REVIEW"]["~VALUE"], 70);
						?><span><?=$show_part?></span><span class="shower">...<br/><span class="shower-text">Читать весь отзыв</span></span><span class="hidden_part"><?=$hidden_part?>
						<?}else{?><?=$item["PROPERTIES"]["REVIEW"]["~VALUE"]?>
						<?}?>						
					</p>	
				<?}?>
			</div>
		<?}?>
	</div>
	<a href="/reviews/" class="show-all">Все отзывы</a>
	<div class="clear"></div>
</div>

