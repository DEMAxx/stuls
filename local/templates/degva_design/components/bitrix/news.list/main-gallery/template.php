<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>
<h1 class="hidden"><? $APPLICATION->ShowTitle(false) ?></h1>
<?php if(!empty($arResult["ITEMS"])){ ?>
<div class="banner-box">
    <div class="gallery-banners">
        <?
        $first = true;
        foreach ($arResult["ITEMS"] as $key => $item) {
            if ($item["PROPERTIES"]["BANNER_IS_LINK"]["VALUE"] == "Y" && strlen(
                    $item["PROPERTIES"]["LINK"]["VALUE"]
                ) > 0) { ?>
                <a <?=$item["PROPERTIES"]["BACKGROUND_COLOR"]["VALUE"]?'style="background-color: '.$item["PROPERTIES"]["BACKGROUND_COLOR"]["VALUE"].';" ':''?>class="item gallery-banners-item-<?=$key?>" href="<?=$item["PROPERTIES"]["LINK"]["VALUE"]?>">
                    <? if (!empty($item['~PREVIEW_TEXT'])) { ?>
                        <? if ($first) { ?>
                            <?=!empty(trim($item["NAME"]))?'<h1>'.$item["NAME"].'</h1>':''?>
                            <? $first = false;
                        } else { ?>
                            <?=!empty(trim($item["NAME"]))?'<h5>'.$item["NAME"].'</h5>':''?>
                        <? } ?>
                        <div class="under_title_text"><?=$item['~PREVIEW_TEXT']?></div>
                        <? if (!empty($item['~DETAIL_TEXT'])) { ?>
                            <div class="helper-wrapper">
                                <div class="help-icon"></div>
                                <div class="help-text"><?=$item['~DETAIL_TEXT']?></div>
                            </div>
                        <? } ?>
                    <? } ?>
                    <style>
                        <?if(!empty($item["DETAIL_PICTURE"]["SRC"])){?>
                        .gallery-banners-item-<?=$key?> {
                            background-image: url(<?=$item["DETAIL_PICTURE"]["SRC"]?>);
                        }
                        @media only screen and (min-width: 960px) {
                            .gallery-banners-item-<?=$key?> {
                                background-image: url(<?=$item["PREVIEW_PICTURE"]["SRC"]?>);
                            }
                        }
                        <?}else{?>
                        .gallery-banners-item-<?=$key?> {
                            background-image: url(<?=$item["PREVIEW_PICTURE"]["SRC"]?>);
                        }
                        <?}?>
                    </style>
                </a>
            <? } else { ?>
                <div <?=$item["PROPERTIES"]["BACKGROUND_COLOR"]["VALUE"]?'style="background-color: '.$item["PROPERTIES"]["BACKGROUND_COLOR"]["VALUE"].';" ':''?> class="item gallery-banners-item-<?=$key?>">
                    <? if (strlen(trim($item["NAME"])) > 0) { ?>
                        <? if ($first) { ?>
                            <?=!empty(trim($item["NAME"]))?'<h1>'.$item["NAME"].'</h1>':''?>
                            <? $first = false;
                        } else { ?>
                            <?=!empty(trim($item["NAME"]))?'<h5>'.$item["NAME"].'</h5>':''?>
                        <? } ?>
                        <? if (!empty($item['~PREVIEW_TEXT'])) { ?>
                            <div class="under_title_text"><?=$item['~PREVIEW_TEXT']?></div>
                            <? if (!empty($item['~DETAIL_TEXT'])) { ?>
                                <div class="helper-wrapper">
                                    <div class="help-icon"></div>
                                    <div class="help-text"><?=$item['~DETAIL_TEXT']?></div>
                                </div>
                            <? } ?>
                        <? } ?>
                        <? if (strlen($item["PROPERTIES"]["LINK"]["VALUE"]) > 0) { ?>
                            <a href="<?=$item["PROPERTIES"]["LINK"]["VALUE"]?>"
                               class="button"><?=$item["PROPERTIES"]["LINK"]["DESCRIPTION"]?></a>
                        <? } ?>
                    <? } ?>
                    <style>
                        <?if(!empty($item["DETAIL_PICTURE"]["SRC"])){?>
                        .gallery-banners-item-<?=$key?> {
                            background-image: url(<?=$item["DETAIL_PICTURE"]["SRC"]?>);
                        }
                        @media only screen and (min-width: 960px) {
                            .gallery-banners-item-<?=$key?> {
                                background-image: url(<?=$item["PREVIEW_PICTURE"]["SRC"]?>);
                            }
                        }
                        <?}else{?>
                        .gallery-banners-item-<?=$key?> {
                            background-image: url(<?=$item["PREVIEW_PICTURE"]["SRC"]?>);
                        }
                        <?}?>
                    </style>
                </div>
            <? } ?>
        <? } ?>
    </div>
    <div class="clear"></div>
</div>
<?php } ?>