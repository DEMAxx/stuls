<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="share-block hit-new center1200">
	<div class="switcher"><h3 class="h1 item">Хиты продаж</h3><span class="delimer">|</span><h3 class="h3 item">Новинки</h3></div>	
	<div class="hit-sallers active">
		<?if (is_array($arResult['ITEMS']) && count($arResult['ITEMS']) > 0){?>
			<div class="product-list gallery-hit">
				<?foreach ($arResult['ITEMS'] as $arItem){?>
                    <?
                    $file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]['ID'], array('width'=>500, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true, \Stuls\Helper::WMParams());
                    ?>
					<a class="product" href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<div class="img"<?if($file['src']):?> style="background-image: url(<?=$file['src']?>)"<?endif;?>></div>
						<?if($arItem["PROPERTIES"]["SHARE"]["VALUE"]=="Да"){?>
							<div class="discount-box">
								<div class="share text"><?=$arItem["PROPERTIES"]["SHARE"]["NAME"]?></div>
								<?if($arItem["PRICE"]["DISCOUNT"]["VALUE"]){?>
									<div class="discount text">-<?=$arItem["PRICE"]["DISCOUNT"]["VALUE"]?>&nbsp;<?=$arItem["PRICE"]['SYMBOL']?></div>
								<?}?>
							</div>
						<?}
						unset($file);
						?>
						<h5><?=$arItem['NAME']?></h5>	
						<div class="price-box">
							<div class="price text"><?=$arItem["PRICE"]["DISCOUNT_PRICE"]?> <span class="rub"></span></div>
							<?if($arItem["PRICE"]["DISCOUNT"]["VALUE"]){?>
								<div class="old-price text"><?=$arItem["PRICE"]["RESULT_PRICE"]["BASE_PRICE"]?> <span class="rub"></span></div>
							<?}else{
								if($arItem["PROPERTIES"]["SHARE_PRICE"]["VALUE"]>0){?>
									<div class="old-price text"><?=$arItem["PROPERTIES"]["SHARE_PRICE"]["VALUE"]?> <span class="rub"></span></div>
								<?}?>
							<?}?>
						</div>
					</a> 
				<?}?>			
			</div>
			<div class="clear"></div>
			<div class="counter hit"><span class="num"></span></div>		
			<a href="/catalog/" class="link"><span class="text">Смотреть все</span><span class="simblol"></span></a>
		<?}?>
	</div>
	<div class="new-sallers">
		<?if (is_array($arResult['NEW']) && count($arResult['NEW']) > 0){?>
			<div class="product-list gallery-new">
				<?foreach ($arResult['NEW'] as $arItem){
					//pre($arItem["PROPERTIES"]["SHARE"]);

                    $file = CFile::ResizeImageGet(
                        $arItem["PREVIEW_PICTURE"],
                        array('width' => 500, 'height' => 500),
                        BX_RESIZE_IMAGE_PROPORTIONAL,
                        true,
                        \Stuls\Helper::WMParams()
                    );

                    ?>
					<a class="product" href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<div class="img"<?if($file['src']):?> style="background-image: url(<?=$file['src']?>)"<?endif;?>></div>
						<?if($arItem["PROPERTIES"]["SHARE"][0]["VALUE_ENUM"]=="Да"){?>
							<div class="discount-box">
								<div class="share text"><?=$arItem["PROPERTIES"]["SHARE"][0]["NAME"]?></div>
								<?if($arItem["PRICE"]["DISCOUNT"]["VALUE"]){?>
									<div class="discount text">-<?=$arItem["PRICE"]["DISCOUNT"]["VALUE"]?>%</div>
								<?}?>
							</div>
						<?}
						unset($file);
						?>
						<h5><?=$arItem['NAME']?></h5>	
						<div class="price-box">
							<div class="price text"><?=$arItem["PRICE"]["DISCOUNT_PRICE"]?> <span class="rub"></span></div>
							<?if($arItem["PRICE"]["DISCOUNT"]["VALUE"]){?>
								<div class="old-price text"><?=$arItem["PRICE"]["RESULT_PRICE"]["BASE_PRICE"]?> <span class="rub"></span></div>
							<?}else{
								if($arItem["PROPERTIES"]["SHARE_PRICE"][0]["VALUE"]>0){?>
									<div class="old-price text"><?=$arItem["PROPERTIES"]["SHARE_PRICE"][0]["VALUE"]?> <span class="rub"></span></div>
								<?}?>
							<?}?>
						</div>
					</a> 
				<?}?>			
			</div>
			<div class="clear"></div>
			<div class="counter new"><span class="num"></span></div>		
			<a href="/catalog/" class="link"><span class="text">Смотреть все</span><span class="simblol"></span></a>
		<?}?>
	</div>
</div>