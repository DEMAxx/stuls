<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $USER;

if(CModule::IncludeModule("iblock"))
{
   $items = GetIBlockElementListEx("Catalog", "catalog", Array(), 
		Array("DATE_ACTIVE_FROM"=>"ASC", "SORT"=>"ASC", "NAME" => "ASC"), 8);
   while($arItem = $items->GetNext())
   {
      $arResult['NEW'][$arItem["ID"]] = $arItem;
      $db_props = CIBlockElement::GetProperty(2, $arItem["ID"], array("sort" => "asc"), Array());
      if($ar_props = $db_props->Fetch()){
         $arResult['NEW'][$arItem["ID"]]["PROPERTIES"][$ar_props["CODE"]][] = $ar_props;
      }
		$arResult['NEW'][$arItem["ID"]]["PRICE"] = CCatalogProduct::GetOptimalPrice($arItem["ID"], 1, $USER->GetUserGroupArray(), 'N');
   }
}
foreach ($arResult['ITEMS'] as $key=>$arItem)
{	
	$arResult['ITEMS'][$key]["PRICE"] = CCatalogProduct::GetOptimalPrice($arItem["ID"], 1, $USER->GetUserGroupArray(), 'N');
    if((int)$arItem["PRICE"]['RESULT_PRICE']['BASE_PRICE'] - (int)$arItem["PRICE"]['RESULT_PRICE']['DISCOUNT_PRICE'] === (int)$arItem["PRICE"]["DISCOUNT"]["VALUE"]){
        $arResult['ITEMS'][$key]["PRICE"]['SYMBOL'] = '<span class="rub"></span>';
    }else{
        $arResult['ITEMS'][$key]["PRICE"]['SYMBOL'] = '%';
    }
}