<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>
<div class="objects center1200">
	<?foreach($arResult["ITEMS"] as $item){?>
		<?//pre($item["PROPERTIES"]);?>
		<div class="item">
			<?$file = CFile::ResizeImageGet($item["PREVIEW_PICTURE"], array('width'=>1200, 'height'=>960), BX_RESIZE_IMAGE_EXACT, true, \Stuls\Helper::WMParams());?>
			<div class="img-box left-picture"><img class="img" src="<?=$file["src"]?>" alt="<?=$item["NAME"]?>" title="<?=$item["NAME"]?>"/></div>
			<div class="description">
				<h3 class="h2"><?=$item["NAME"]?></h3>
				<p class="text address"><?=$item["PROPERTIES"]["ADRESS"]["VALUE"]?></p>
				<p class="text"><?=$item["PREVIEW_TEXT"]?></p>
				<?if($item["PROPERTIES"]["BUTTON"]["VALUE"]=="Нужна"){?>
					<a href="<?=$item["DETAIL_PAGE_URL"]?>" class="button">Подробнее</a>
				<?}?>
			</div>			
		</div>
	<?}?>
</div>
