<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="share-block center1200">
	<h3 class="h1">Акции</h3>
	<?if (is_array($arResult['ITEMS']) && count($arResult['ITEMS']) > 0){?>
		<div class="product-list gallery-share">
			<?foreach ($arResult['ITEMS'] as $arItem){
                $file = CFile::ResizeImageGet(
                    $arItem["PREVIEW_PICTURE"]['ID'],
                    array('width' => 500, 'height' => 500),
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    true,
                    \Stuls\Helper::WMParams()
                );
			    ?>
				<a class="product" href="<?=$arItem['DETAIL_PAGE_URL']?>">
					<div class="img"<?if($file['src']):?> style="background-image: url(<?=$file['src']?>)"<?endif;?>></div>
					<?if(($arItem["PROPERTIES"]["SHARE"]["VALUE"]=="Да")||$arItem["PRICE"]["DISCOUNT"]["VALUE"]){?>
						<div class="discount-box">
							<?if($arItem["PROPERTIES"]["SHARE"]["VALUE"]=="Да"){?>
								<div class="share text"><?=$arItem["PROPERTIES"]["SHARE"]["NAME"]?></div>
							<?}?>
							<?if($arItem["PRICE"]["DISCOUNT"]["VALUE"]){?>
								<div class="discount text">-<?=$arItem["PRICE"]["DISCOUNT"]["VALUE"]?>
                                    <?if($arItem["PRICE"]["DISCOUNT"]["VALUE_TYPE"] == 'P'){
                                        echo '%';
                                    }else{
                                        echo '<span class="rub"></span>';
                                    }
                                    ?>
                                </div>
							<?}?>
						</div>
					<?}?>
					<h5><?=$arItem['NAME']?></h5>	
					<div class="price-box">
						<div class="price text"><?=$arItem["PRICE"]["DISCOUNT_PRICE"]?> <span class="rub"></span></div>
						<?if($arItem["PRICE"]["DISCOUNT"]["VALUE"]){?>
							<div class="old-price text"><?=$arItem["PRICE"]["RESULT_PRICE"]["BASE_PRICE"]?> <span class="rub"></span></div>
						<?}else{
							if($arItem["PROPERTIES"]["SHARE_PRICE"]["VALUE"]>0){?>
								<div class="old-price text"><?=$arItem["PROPERTIES"]["SHARE_PRICE"]["VALUE"]?> <span class="rub"></span></div>
							<?}?>
						<?}?>
					</div>
				</a> 
			<?}?>			
		</div>
		<div class="clear"></div>
		<div class="counter c-share"><span class="num"></span></div>		
		<a href="/catalog/0/filter/share-is-1/apply/" class="link"><span class="text">Смотреть все</span><span class="simblol"></span></a>
	<?}?>
</div>
<?//pre($arResult['ITEMS']);?>