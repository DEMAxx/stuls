<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>
<div class="reviews-block reviews-page center1200">
	<?$i = 0;?>
	<div class="reviews-list">
		<?foreach($arResult["ITEMS"] as $key=>$item){?>
			<?//pre($item);
			$estimation = round(($item["PROPERTIES"]["SHOP_ESTIMATION"]["VALUE_XML_ID"]+$item["PROPERTIES"]["STAFF_ESTIMATION"]["VALUE_XML_ID"]+$item["PROPERTIES"]["DELIVERY_ESTIMATION"]["VALUE_XML_ID"])/3);
			?>
			<div class="review<?if($key>4){?> hide<?}?><?if($key==4){?> last<?}?>">
				<div class="date"><?if($item["DISPLAY_ACTIVE_FROM"]){?><?=$item["DISPLAY_ACTIVE_FROM"]?><?}else{?><?=$item["PROPERTIES"]["DATE"]["VALUE"]?><?}?></div>
				<h5><?=$item["PROPERTIES"]["NAME"]["VALUE"]?></h5>
				<div class="estimation">
					<?for ($i=0; $i < 5; $i++) {?>
						<div class="star<?if($i<$estimation){echo " active";}?>"><svg viewbox="0 0 475.075 475.075"><use xlink:href="#star"/></svg></div>
					<?}?>	
				</div>
				<?if($item["PROPERTIES"]["REVIEW"]["VALUE"]){?>
					<p class="text"><?=$item["PROPERTIES"]["REVIEW"]["VALUE"]?></p>	
				<?}?>
			</div>
			<?$i++;?>
		<?}?>
	</div>
	<?if($i>4){?>
		<div class="center-block">
			<span class="show-all">Смотреть все</span>
		</div>		
	<?}?>
	<div class="set-review-block">
		<div class="info">
			<img src="/images/svg/chat.svg" title="Вопрос" alt="Вопрос" class="img">
			<p class="info-text">Оставить комментарий&nbsp;вы&nbsp;сможете только после покупки</p>
		</div>
			<div data-id="5" class="button set-review">Оставить отзыв</div>
	</div>	
	<div class="clear"></div>
</div>

