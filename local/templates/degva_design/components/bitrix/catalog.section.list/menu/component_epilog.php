<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();

?>
    <div class="brown-menu-cover">
        <div class="center1200">
            <? foreach ($arResult["MENU"] as $section) { ?>
                <div class="section-wrapper">
                    <a href="<?=$section['MAIN_SECTION']["SECTION_PAGE_URL"]?>"
                       class="item<? if ($section['MAIN_SECTION']["SECTION_PAGE_URL"] == $dir) {
                           echo ' active';
                       } ?>">
                        <h5 class="h5"><?=$section['MAIN_SECTION']["NAME"]?></h5>
                    </a>
                    <? if (!empty($section['SUB'])) { ?>
                        <span class="arrow-down"></span>
                        <div class="sub-menu">
                            <? foreach ($section['SUB'] as $sub) { ?>
                                <a href="<?=$sub["SECTION_PAGE_URL"]?>"
                                   class="item<? if ($sub["SECTION_PAGE_URL"] == $dir) {
                                       echo ' active';
                                   } ?>">
                                    <h5 class="h5"><?=$sub["NAME"]?></h5>
                                </a>
                            <? } ?>
                        </div>
                    <? } ?>
                </div>
            <? } ?>
            <div class="section-wrapper action">
                <a href="/catalog/0/filter/share-is-1/apply/"
                   class="item<? if ($dir == "/catalog/0/filter/share-is-1/apply/") {
                       echo ' active';
                   } ?> share">
                    <h5 class="h5">Акции</h5>
                </a>
            </div>
        </div>
    </div>
<? //pre($arResult);?>