<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global \CMain $APPLICATION */
/** @global \CUser $USER */
/** @global \CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

global $APPLICATION;

$menu = [];

foreach ($arResult["SECTIONS"] as $section){
    if($section['DEPTH_LEVEL'] == 1){
        $menu[$section["ID"]]['MAIN_SECTION'] = ['NAME' => $section["NAME"], 'SECTION_PAGE_URL' => $section["SECTION_PAGE_URL"]];
    }else{
        $menu[$section['IBLOCK_SECTION_ID']]['SUB'][] = ['NAME' => $section["NAME"], 'SECTION_PAGE_URL' => $section["SECTION_PAGE_URL"]];
    }
}

$cp = $this->__component; // объект компонента

if (is_object($cp))
{
    // добавим в arResult компонента SECTIONS
    $cp->arResult['MENU'] = $menu;
    $cp->SetResultCacheKeys(array('MENU'));
    // сохраним их в копии arResult, с которой работает шаблон
    $arResult['MENU'] = $cp->arResult['MENU'];
}