<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$IBLOCK = 7;
foreach($arResult["SECTIONS"] as $key=>$section){
    $arSelect = Array("ID", "NAME", "PROPERTY_PICTURE", "PROPERTY_TEXT_BOTTOM");
    $arFilter = Array("IBLOCK_ID"=>IntVal($IBLOCK), "SECTION_ID"=>$section["ID"], "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    while($ob = $res->GetNext())
    {
        $ob["PICTURE"] = CFile::GetPath($ob["PROPERTY_PICTURE_VALUE"]);
        $arResult["SECTIONS"][$key]["ELEMENTS"][] = $ob;
    }

}
?>
