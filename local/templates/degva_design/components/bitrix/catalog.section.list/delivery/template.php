<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>
<div class="center1200 delivery-wrapper">	
	<?foreach($arResult["SECTIONS"] as $section){
		?>
		<div class="ib-3-wrapper">
			<div class="center-block">
				<h2 class="title"><span class="text-shadow underline"><?=$section["NAME"]?>:</span></h2>
			</div>
			<?foreach($section["ELEMENTS"] as $element){?>
				<div class="ib-3<?if($element["PROPERTY_TEXT_BOTTOM_ENUM_ID"]==14){?> text-bottom<?}?>">
					<img src="<?=$element["PICTURE"]?>" title="<?=$element["NAME"]?>" alt="<?=$element["NAME"]?>" class="img">
					<p class="text"><?=$element["NAME"]?></p> 
				</div>
			<?}?>
			<p class="description"><?=$section["~DESCRIPTION"]?></p>			
		</div>
	<?}?>	
</div>
<?//pre($arResult);?>