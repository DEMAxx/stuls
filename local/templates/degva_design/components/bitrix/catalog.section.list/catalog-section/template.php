<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);

?>
<div class="center1200 brand-main<?if($arParams["DISPLAY_LINE"]=="Y"){?> inline_6<?}?>">
	<?if($arParams["CUSTOM_TITLE"]=="Y"){?>
		<h3 class="<?if($arParams["OTHER_PAGE"]=="Y"){echo "on-detail ";}?>h1">Категории товаров</h3>
	<?}?>
	<div class="ib-2-wrapper">
		<?foreach($arResult["SECTIONS"] as $section){
            $file = CFile::ResizeImageGet($section["PICTURE"], array('width'=>464, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			?>
			<a href="<?=$section["SECTION_PAGE_URL"]?>" class="item ib-2 ib-3 ib-4">
				<h5 class="h5<?if($section["NAME"]=="Акции"){echo " yellow-box";}else{?><?}?>"><?=$section["NAME"]?></h5>
				<div class="background" style="background-image:url(<?=$file["src"]?>)"></div>
			</a>
		<?}?>
        <? if($arParams['SHOW_ACTION'] == 'Y'){ ?>
            <a href="/catalog/0/filter/share-is-1/apply/" class="item ib-2 ib-3 ib-4">
                <h5 class="h5 yellow-box">Акции</h5>
                <div class="background" style="background-image:url('<?=SITE_TEMPLATE_PATH?>/images/action-stuls.png')"></div>
            </a>
        <? } ?>
	</div>
	<?if($arParams["CUSTOM_TITLE"]=="Y"){?>
		<?if(!$arParams["DISPLAY_LINE"]=="Y"){?>
			<a href="/catalog/" class="link"><span class="text">Перейти в каталог</span><span class="simblol"></span></a>
		<?}?>
	<?}?>
</div>
<?//pre($arResult);?>