<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global \CMain $APPLICATION */
/** @global \CUser $USER */
/** @global \CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

$IDs = array_column($arResult["SECTIONS"], 'ID');

foreach($arResult["SECTIONS"] as $section){
    if($section['IBLOCK_SECTION_ID'] && $section['IBLOCK_SECTION_ID']>0){
        $findParent = array_search($section['IBLOCK_SECTION_ID'], $IDs);
        if(!empty($arResult["SECTIONS"][$findParent])){
            unset($arResult["SECTIONS"][$findParent]);
        }
        unset($findParent);
    }
}
