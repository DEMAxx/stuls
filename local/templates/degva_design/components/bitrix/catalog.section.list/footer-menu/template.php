<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>
<?if (!empty($arResult)):?>		
	<div class="menu bottom">
		<div class="clear"></div>
		<div class="title">Каталог</div>
		<?foreach($arResult["SECTIONS"] as $section){?>
			<div class="item_wrapper">
				<a href="<?=$section["SECTION_PAGE_URL"]?>" class="item"><?=$section["NAME"]?></a>
			</div>
		<?}?>
	</div>
<?endif?>
<?//pre($arResult);?>