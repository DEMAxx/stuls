<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>
<form class="form" name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
	<h3><?=$arParams["CUSTOM_TITLE_NAME"]?></h3>
	<?
	if (!empty($arResult["ERRORS"])):?>
		<?ShowError(implode("\n", $arResult["ERRORS"]))?>
		<script>
			$(document).ready(function () {
					$('.star').hover(function(){
					var rating = $(this).data('rating');
					var block=$(this).parents('.rating-line');
					$('.star',block).each(function(key,val){
						if(key<rating){
							$(this).addClass("on");
						}
					});
				},function(){
					var rating = $(this).data('rating');
					var block=$(this).parents('.rating-line');
					$('.star',block).each(function(key,val){
						if(key<rating){
							$(this).removeClass("on");
						}
					});
				});
				$('.star').click(function(){
					var block=$(this).parents('.inp-box');
					var blockLine=$(this).parents('.rating-line');
					var vote=$(this).data('rating');
					$('span',block).each(function(){
						var voteText = $(this).data('rating');
						if(voteText == vote){
							$(this).removeClass('hidden');
						}else{
							$(this).addClass('hidden');
						}
					});
					$('.star',blockLine).each(function(key,val){
						if(key<vote){
							$(this).addClass("active");
						}else{
							$(this).removeClass("active");
						}
					});
				});
			});
		</script>
	<?endif;
	if (strlen($arResult["MESSAGE"]) > 0){?>
		<div class="seccess-answer"><?ShowNote($arResult["MESSAGE"])?></div>
	<?}else{?>
	<?$arNamedElems=array();
	foreach($arResult["PROPERTY_LIST"] as $k=>$propertyID){
		if (!(intval($propertyID) > 0)){
			$arNamedElems[$propertyID]=$propertyID;
			unset($arResult["PROPERTY_LIST"][$k]);
		}/*elseif(){
		
		}*/
	}
	?>
	<?=bitrix_sessid_post()?>
	<?//pre($arParams);
	$name_string='';
	if($arParams["CUSTOM_TITLE_NAME"])
		$name_string = $arParams["CUSTOM_TITLE_NAME"].' '.GetMessage('FROM').' ';
	$name_string .= date('d.m.Y H:i');
	//if($arParams["TYPE_ID"]) $name_string .= ' ELEM_ID='.$arParams["TYPE_ID"];
	if($arParams["BASE_IBLOCK_ID"]){?>
		<div class="hidden"><input type="text" value="<?=$arParams["BASE_IBLOCK_ID"]?>" name="BASE_IBLOCK_ID"/></div>
	<?}
	?>
	<div class="hidden"><input type="text" value="<?=$name_string?>" name="PROPERTY[NAME][0]"/></div>
	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
	<?if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])){?>
		<?foreach ($arResult["PROPERTY_LIST"] as $propertyID){
			$dataType='input text';
			if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='email'){$dataType='input mail';}elseif($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='phone'){$dataType='input phone';}
			$addClass='';
			$specialClass='';
			if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='services'){
				$specialClass=' services';
			}elseif($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='file'){
				$specialClass=' file';
			}elseif($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='agreement'){
				$specialClass=' agreement';
			}elseif($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='requirements'){
				$specialClass=' requirements';
			}elseif($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='comment'){
				$specialClass=' comment';
			}else{
				$specialClass=' text-block';
			}
			$raiting = false;
			if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="SHOP_ESTIMATION"||$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="STAFF_ESTIMATION"||$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="DELIVERY_ESTIMATION"){
				$raiting = true;
				$specialClass=' rating-block';
			}
			if($arResult["PROPERTY_LIST_FULL"][$propertyID]['ERROR']) $addClass.=' error';
			if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])) $addClass.=' required';
		?>
		<?if($arParams["IBLOCK_ID"]==3&&$arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='name'){?>
			<label class="title" for="f-item_<?=$propertyID?>">Заполните данные</label>
		<?}?>
		<div class="item<?=$addClass?><?=$specialClass?>">
			<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] != "S"){?>
				<label for="f-item_<?=$propertyID?>">
			
				<?if (intval($propertyID) > 0){
					if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]!="agreement"){
						echo $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];
					}
				}else{
					echo (!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID));
				}?>
				</label>
			<?}else{?>
				<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]!="agreement"){?>
					<label for="f-item_<?=$propertyID?>">
						<?
						echo $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];
						?>
					</label>
				<?}?>
			<?}?>
			<?if (intval($propertyID) > 0){
				if (
					$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
					&&
					$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
				){$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
				}elseif (
					($arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
					||
					$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
					)&&$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
				){$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";}
			}elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
				$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

				if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
				{
					$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
					$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
				}
				else
				{
					$inputNum = 1;
				}

				if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
					$INPUT_TYPE = "USER_TYPE";
				else
					$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

				switch ($INPUT_TYPE):
					case "USER_TYPE":
						for ($i = 0; $i<$inputNum; $i++)
						{
							if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
							{
								$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
								$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
							}
							elseif ($i == 0)
							{
								$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
								$description = "";
							}
							else
							{
								$value = "";
								$description = "";
							}echo '<div class="inp-date">';
							echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
								array(
									$arResult["PROPERTY_LIST_FULL"][$propertyID],
									array(
										"VALUE" => $value,
										"DESCRIPTION" => $description,
									),
									array(
										"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
										"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
										"FORM_NAME"=>"iblock_add",
									),
								));echo '</div>';
						?><?
						}
					break;
					case "TAGS":
						$APPLICATION->IncludeComponent(
							"bitrix:search.tags.input",
							"",
							array(
								"VALUE" => $arResult["ELEMENT"][$propertyID],
								"NAME" => "PROPERTY[".$propertyID."][0]",
								"TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
							), null, array("HIDE_ICONS"=>"Y")
						);
						break;
					case "HTML":
						$LHE = new CLightHTMLEditor;
						$LHE->Show(array(
							'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
							'width' => '100%',
							'height' => '200px',
							'inputName' => "PROPERTY[".$propertyID."][0]",
							'content' => $arResult["ELEMENT"][$propertyID],
							'bUseFileDialogs' => false,
							'bFloatingToolbar' => false,
							'bArisingToolbar' => false,
							'toolbarConfig' => array(
								'Bold', 'Italic', 'Underline', 'RemoveFormat',
								'CreateLink', 'DeleteLink', 'Image', 'Video',
								'BackColor', 'ForeColor',
								'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
								'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
								'StyleList', 'HeaderList',
								'FontList', 'FontSizeList',
							),
						));
						break;
					case "T":
						for ($i = 0; $i<$inputNum; $i++)
						{

							if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
							{
								$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
							}
							elseif ($i == 0)
							{
								$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
							}
							else
							{
								$value = "";
							}
						?>
				<textarea data-type="text" id="f-item_<?=$propertyID?>" placeholder="<?//echo $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];?>" cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
						<?
						}
					break;

					case "S":
					case "N":
						for ($i = 0; $i<$inputNum; $i++)
						{
							if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
							{
								$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
							}
							elseif ($i == 0)
							{
								$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

							}
							else
							{
								$value = "";
							}
						?>
						<input placeholder="<?//echo $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];?>" class="<?=$dataType?>" id="f-item_<?=$propertyID?>" type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" />
						<?
						if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><?
							$APPLICATION->IncludeComponent(
								'bitrix:main.calendar',
								'',
								array(
									'FORM_NAME' => 'iblock_add',
									'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
									'INPUT_VALUE' => $value,
									'SHOW_TIME' => 'N',
									'HIDE_TIMEBAR' => 'N' 
								),
								null,
								array('HIDE_ICONS' => 'Y')
							);
							?><small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small><?
						endif;
						/*?><?*/
						}
					break;

					case "F":
								for ($i = 0; $i<$inputNum; $i++)
								{
									$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									?>
						<input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
						<input type="file" id="file_<?=$propertyID?>" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" />
						<label for="file_<?=$propertyID?>"><span class="set-file">Прикрепите файл</span></label>
						<br />
									<?

									if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
									{
										?>
					<input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label><br />
										<?

										if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
										{
											?>
					<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" /><br />
											<?
										}
										else
										{
											?>
					<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
					<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
					[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
											<?
										}
									}
								}

							break;
					case "L":

						if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
							$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
						else
							$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

						switch ($type):
							case "checkbox":
							case "radio":
								$raiting_num = 1;								
								if($raiting){?>
									<div class="rating-line">
								<?}
								foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
								{	
									$checked = false;
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
										{
											foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
											{
												if ($arElEnum["VALUE"] == $key)
												{
													$checked = true;
													break;
												}
											}
										}
									}
									else
									{
										if ($arEnum["DEF"] == "Y") $checked = true;
									}
									

									?>
									
									<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="agreement"){?>
										<input data-type="<?=$dataType?>" name="PROPERTY[<?=$propertyID?>]" type='checkbox' value="<?=$key?>" id="property_<?=$key?>" checked />
										<label for="property_<?=$key?>" class="policy">Согласен на обработку своих персональных данных в соответствии с <a href="/upload/Пользовательское-соглашение.pdf" target="_blank">Условиями.</a><?/*<br><a href="/upload/Политика-конфиденциальности.pdf" target="_blank">Политика конфиденциальности</a>*/?></label>
									<?}else{?>
									<input type="<?=$type?>" data-type="<?=$dataType?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label <?if($raiting){?>data-rating="<?=$raiting_num?>" <?}?>class="label<?=$specialClass?>_<?=$key?><?if($raiting){?> star<?}?>" for="property_<?=$key?>"><?if($raiting){?><svg viewbox="0 0 475.075 475.075"><use xlink:href="#star"/></svg><?}else{?><?=$arEnum["VALUE"]?><?}?></label>
									<?}	
									$raiting_num++;
								}
								if($raiting){?>
									</div>
								<?}?>
							<?
							break;

							case "dropdown":
							case "multiselect":
							?>
					<select id="f-item_<?=$propertyID?>" name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
						<option value=""><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
							<?
								if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
								else $sKey = "ELEMENT";

								foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
								{
									$checked = false;
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
										{
											if ($key == $arElEnum["VALUE"])
											{
												$checked = true;
												break;
											}
										}
									}
									else
									{
										if ($arEnum["DEF"] == "Y") $checked = true;
									}
									?>
						<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
									<?
								}
							?>
					</select>
							<?
							break;

						endswitch;
					break;
				endswitch;?>
		</div>
		<?}?>
		<?if(isset($arNamedElems['PREVIEW_TEXT'])){
			$propertyID=$arNamedElems['PREVIEW_TEXT'];
		?>
		<div class="item<?if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])) echo ' required';?>">
			<label for="f-item_PREVIEW_TEXT">
			<?echo (!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID));?>
			</label>
			<textarea data-type="textarea" name="PROPERTY[PREVIEW_TEXT][0]" rows="5" cols="30" id="f-item_PREVIEW_TEXT"></textarea>
		</div>
		<?}?>
		<?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0){?>
		<div class="captcha-box item required">
			<div class="left">
				<label for="f-item_CAPTCHA">Введите символы с картинки</label>
				<input class="captcha_sid" type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
				<input id="f-item_CAPTCHA" class="captcha_word"  data-type="captcha" type="text" name="captcha_word" maxlength="50" value="">
			</div>
			<div class="left">
				<a href="#" class="reload-captcha" data-folder="<?=$this->__folder?>">Обновить</a>
				<img class="captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
			</div>
		</div>
		<?}?>
	<?}?>
	<?}?>
	<div class="clear"></div>
	<?if (strlen($arResult["MESSAGE"]) > 0){?>
		<div class="submit-box">
			<input type="submit" class="button ok red-btn" name="iblock_submit" value="OK" />
		</div>
	<?}else{?>
		<div class="submit-box">
			<input type="submit" class="button red-btn" name="iblock_submit" value="<?=($arParams["CUSTOM_BUTTON_TITLE"]?$arParams["CUSTOM_BUTTON_TITLE"]:'Отправить')?>" />
		</div>
	<?}?>
	<?if (strlen($arParams["LIST_URL"]) > 0){?>
		<input type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
		<input
			type="button"
			name="iblock_cancel"
			value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
			onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"])?>';">
	<?}?>
</form>
<?//pre($_REQUEST)?>
