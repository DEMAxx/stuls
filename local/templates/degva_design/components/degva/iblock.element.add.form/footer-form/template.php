<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>
<?//pre($arParams);?>
<?//pre($arResult);?>
<?//pre($_REQUEST);?>
<div class="footer-form-wrapper">
	<div class="bg-coover"></div>
	<form id="form_<?=$arParams['IBLOCK_ID']?>" class="send_request center1200" name="send_request" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
	<?if (strlen($arResult["MESSAGE"]) > 0){?>
		<div class="message_good">
			<?ShowNote($arResult["MESSAGE"]);?>
			<?/*<div class="submit-box">
				<input type="submit" class="button ok" name="iblock_submit" value="OK" />
			</div>*/?>
		</div>
	<?}else{?>
		<input type="hidden" name="id" value="<?=$arParams['BASE_IBLOCK_ID']?>">
			<div class="title">
				<p>Оставьте заявку<br/>на бесплатную консультацию</p>
			</div>
			<?$i=0; foreach($arResult["OBJECT_TYPES"] as $key => $mailInput){?>
				<input type="checkbox" name="e-mail" <?if($i==0)echo "checked='checked'"?> value="<?=$mailInput["VALUE"]?>" data-name="<?=$mailInput["NAME"]?>" class="e-mail hidden"/>
			<?$i++; }?>
			<?$arNamedElems=array();
			foreach($arResult["PROPERTY_LIST"] as $k=>$propertyID){
				if (!(intval($propertyID) > 0)){
					$arNamedElems[$propertyID]=$propertyID;
					unset($arResult["PROPERTY_LIST"][$k]);
				}/*elseif(){
				
				}*/
			}
			?>
			<?=bitrix_sessid_post()?>
			<?//spre($arResult);
			$name_string='';
			if(!empty($arParams['CUSTOM_TITLE_NAME'])){
				$name_string = $arParams['CUSTOM_TITLE_NAME'].': '.$_REQUEST['title'].' '.GetMessage('FROM').' ';
				$name_string .= date('d.m.Y H:i');
			//if($arParams["TYPE_ID"]) $name_string .= ' ELEM_ID='.$arParams["TYPE_ID"];
			}else{
				$name_string = $arParams['TITLE_NAME'].': '.$_REQUEST['title'].' '.GetMessage('FROM').' ';
				$name_string .= date('d.m.Y H:i');
			}
			?>
			<div><input  type="hidden" value="<?=$name_string?>" name="PROPERTY[NAME][0]"/></div>
			
				
			<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
			<?if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])){?>
				<?foreach ($arResult["PROPERTY_LIST"] as $propertyID){
					$addClass='';
					if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='COMMENT'){
						$addClass=' textarea';
					}elseif($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='NAME'){
						$addClass=' text';
					}elseif($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='EMAIL'){
						$addClass=' text';
					}elseif($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']=='PHONE'){
						$addClass=' text';
						$arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"]=$arResult['USER']['UF_CEO_NAME'];
					}
					if($arResult["PROPERTY_LIST_FULL"][$propertyID]['ERROR']) $addClass.=' error';
					if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])) $addClass.=' required';
				?>
				<div class="item i-item_<?=$propertyID?><?=$addClass?>">
							<?/*<label class="l-item_<?=$propertyID?>" for="f-item_<?=$propertyID?>">
								<?if (intval($propertyID) > 0){
									echo $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];
								}else{
									echo (!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID));
								}?>
							</label>*/?>
					<?if (intval($propertyID) > 0){
							if (
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
								&&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
							){$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
							}elseif (
								($arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
								||
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
								)&&$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
							){$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";}
					}elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
						$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

						if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
						{
							$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
							$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
						}
						else
						{
							$inputNum = 1;
						}

						if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
							$INPUT_TYPE = "USER_TYPE";
						else
							$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];
						switch ($INPUT_TYPE):
							case "USER_TYPE":
								
								?><textarea data-folder="2" name="PROPERTY[<?=$propertyID?>][0]" value="" id="f-item_<?=$propertyID?>"></textarea><?
								
								
							break;
							case "TAGS":
								$APPLICATION->IncludeComponent(
									"bitrix:search.tags.input",
									"",
									array(
										"VALUE" => $arResult["ELEMENT"][$propertyID],
										"NAME" => "PROPERTY[".$propertyID."][0]",
										"TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
									), null, array("HIDE_ICONS"=>"Y")
								);
								break;
							case "HTML":
								$LHE = new CLightHTMLEditor;
								$LHE->Show(array(
									'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
									'width' => '100%',
									'height' => '200px',
									'inputName' => "PROPERTY[".$propertyID."][0]",
									'content' => $arResult["ELEMENT"][$propertyID],
									'bUseFileDialogs' => false,
									'bFloatingToolbar' => false,
									'bArisingToolbar' => false,
									'toolbarConfig' => array(
										'Bold', 'Italic', 'Underline', 'RemoveFormat',
										'CreateLink', 'DeleteLink', 'Image', 'Video',
										'BackColor', 'ForeColor',
										'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
										'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
										'StyleList', 'HeaderList',
										'FontList', 'FontSizeList',
									),
								));
								break;
							case "T":
								for ($i = 0; $i<$inputNum; $i++)
								{

									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									}
									elseif ($i == 0)
									{
										$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
									}
									else
									{
										$value = "";
									}
								?>
						<textarea data-type="text" id="f-item_<?=$propertyID?>" cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" placeholder="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?>"><?=$value?></textarea>
								<?
								}
							break;

							case "S":
							case "N":
								for ($i = 0; $i<$inputNum; $i++)
								{
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									}
									elseif ($i == 0)
									{
										$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

									}
									else
									{
										$value = "";
									}
								?>
								<input <?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]==''){ echo "class=''";}else{?>class="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]?>"<?}?> data-type="<?=$dataType?>" id="f-item_<?=$propertyID?>" type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" placeholder="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?>" size="25" value="<?=$value?>" />
								<?
								if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><?
									$APPLICATION->IncludeComponent(
										'bitrix:main.calendar',
										'',
										array(
											'FORM_NAME' => 'iblock_add',
											'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
											'INPUT_VALUE' => $value,
											'SHOW_TIME' => 'N',
											'HIDE_TIMEBAR' => 'N' 
										),
										null,
										array('HIDE_ICONS' => 'Y')
									);
									?><small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small><?
								endif;
								/*?><?*/
								}
							break;

							case "F":
								for ($i = 0; $i<$inputNum; $i++)
								{
									$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									?><div class="inp-file">
						<?//<div class="box">?>
							<input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
							<input data-type="file" id="f-item_<?=$propertyID?>" type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" />
						<?//</div>?>
						<label for="f-item_<?=$propertyID?>" class="file-info">
							<?/*<span class="name"></span>
							<span class="type"></span>
							<span class="remove-file"></span>*/?>
							<span class="fake-btn"></span>
							<span class="default">Загрузить файл</span>							
						</label>
									</div>
									<?

									if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
									{
										?>
					<input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label>
										<?

										if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
										{
											?>
					<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" />
											<?
										}
										else
										{
											?>
					<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?>
					<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b
					[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]
											<?
										}
									}
									
								}

							break;
							case "L":

								if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
									$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
								else
									$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

								switch ($type):
									case "checkbox":
									case "radio":
										foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
										{
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
												{
													foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
													{
														if ($arElEnum["VALUE"] == $key)
														{
															$checked = true;
															break;
														}
													}
												}
											}
											else
											{
												if ($arEnum["DEF"] == "Y") $checked = true;
											}
											

											?>
							<?if($propertyID == 37){?>
								<?if($key == 7){?>
									<input class="checkbox_inline" type="checkbox" data-type="<?=$dataType?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> checked='checked' />
								<?}?><label for="property_<?=$key?>" class="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']?>" ><?=$arEnum["~VALUE"]?></label>
							<?}else{?>
								<input class="checkbox_inline" type="checkbox" data-type="<?=$dataType?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> checked='checked' /><label for="property_<?=$key?>" class="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']?>"><?=$arEnum["~VALUE"]?></label>
							<?}?>
											<?
										}
									break;

									case "dropdown":
									case "multiselect":
									?>
							<select id="f-item_<?=$propertyID?>" name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>" class="dropdown">
								<?
										if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
										else $sKey = "ELEMENT";

										foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
										{
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
												{
													if ($key == $arElEnum["VALUE"])
													{
														$checked = true;
														break;
													}
												}
											}
											else
											{
												if ($arEnum["DEF"] == "Y") $checked = true;
											}
											?>
								<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?> <?if($propertyID == 186){?> м&sup2;<?}?></option>
											<?
										}
									?>
							</select>
									<?
									break;

								endswitch;
							break;
						endswitch;?>
				</div>
				<?}?>
				<?if(isset($arNamedElems['PREVIEW_TEXT'])){
					$propertyID=$arNamedElems['PREVIEW_TEXT'];
				?>
				<div class="item<?if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])) echo ' required';?>">
					<label for="f-item_PREVIEW_TEXT">
					<?echo (!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID));?>
					</label>
					<textarea data-type="textarea" name="PROPERTY[PREVIEW_TEXT][0]" rows="5" cols="30" id="f-item_PREVIEW_TEXT"></textarea>
				</div>
				<?}?>
				<?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0){?>
				<div class="captcha-box item required">
					<div class="left">
						<label for="f-item_CAPTCHA">Введите символы с картинки</label>
						<input class="captcha_sid" type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
						<input id="f-item_CAPTCHA" class="captcha_word"  data-type="captcha" type="text" name="captcha_word" maxlength="50" value="">
					</div>
					<div class="left">
						<a href="#" class="reload-captcha" data-folder="<?=$this->__folder?>">Обновить</a>
						<img class="captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
					</div>
				</div>
				<?}?>
			<?}?>
			<?
			if (!empty($arResult["ERRORS"])):?>
				<div class="item"><?ShowError(implode("\n", $arResult["ERRORS"]))?></div>
				<br/>
			<?endif;?>
				<div class="submit-box">
					<input id="loadforms" type="submit" class="button <?if (strlen($arResult["MESSAGE"]) > 1){?>close-button_call<?}?>" name="iblock_submit" <?/*data-newid="300"*/?> value="<?if (strlen($arResult["MESSAGE"]) < 1){?>Отправить<?}else{?>OK<?}?>" />
				</div>
			<?if (strlen($arParams["LIST_URL"]) > 0){?>
				<input type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
				<input
					type="button"
					name="iblock_cancel"
					value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
					onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"])?>';">
			<?}?>
		<?}?>
	</form>
</div>
<?//pre($arResult);?>
<?//pre($_REQUEST);?>



