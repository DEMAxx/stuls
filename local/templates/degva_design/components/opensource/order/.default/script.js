$(document).ready(function () {
    var $locationSearch = $('.location-search');

    $locationSearch.selectize({
        valueField: 'code',
        labelField: 'label',
        searchField: 'label',
        create: false,
        render: {
            option: function (item, escape) {
                return '<div class="title">' + escape(item.label) + '</div>';
            }
        },
        load: function (q, callback) {
            if (!q.length) return callback();

            var query = {
                c: 'opensource:order',
                action: 'searchLocation',
                mode: 'ajax',
                q: q
            };

            $.ajax({
                url: '/bitrix/services/main/ajax.php?' + $.param(query, true),
                type: 'GET',
                error: function () {
                    callback();
                },
                success: function (res) {
                    if (res.status === 'success') {
                        callback(res.data);
                    } else {
                        console.log(res.errors);
                        callback();
                    }
                }
            });
        }
    });
    
    /**
     * Recalculate all deliveries on location prop change
     */
    var deliveryBlock = $('#delivery-variants');
    var deliveryPropsBlock = $('.delivery-properties');

    function renderProperty(property) {
        var html = '';
        /** Собираем HTML свойства и обновляем блок */
        let required = '';
        if(property.REQUIRED=='Y'){required = ' required';}
        let sdekAddr = '';
        if(property.CODE == 'SDEK_ADDR'){
            sdekAddr = ' for-input-sdek hidden';
            $('.show-sdek-map').removeClass('hidden');
            $('.group_id_location ').removeClass('hidden');
        }
        html += '<div class="input-wrapper group_id_'+property.PROPS_GROUP_ID+sdekAddr+' property_'+property.CODE+required+'">';
        html += '<label for="property_'+property.CODE+'">';
        html += property.NAME;
            if(property.REQUIRED=='Y'){
                html += '<span class="required" title="%s">*</span>';
            }
        html += '</label>';

        if(property.TYPE == 'STRING'){
            html += '<input id="property_'+property.CODE+'" type="text" name="properties['+property.CODE+']" value="'+property.VALUE+'">';
        }

        html += '</div>';


        if(property.CODE == 'SDEK_ADDR'){
            deliveryPropsBlock.append(html);
        }else{
            $('.property_CITY ').before($(html));
        }

    }

    function renderTotal(deliveryData){
        $('.delivery-total .delivery-value').html(deliveryData.price);
        var total = $('.no-discont-val').html() - $('.discont-value').html() + deliveryData.price;
        $('.total-val').html(total);
    }

    /** */
    $locationSearch.change(function (event) {
        var locationCode = $(event.target).val();

        console.log(locationCode);

        if (locationCode.length > 0) {

            let city = $('[data-value='+locationCode+']').text().split(',')[0];

            IPOLSDEK_pvz.chooseCity(city);

            var formData = $('#os-order-form').serialize();

            var query = {
                c: 'opensource:order',
                action: 'calculateDeliveries',
                mode: 'ajax'
            };

            var request = $.ajax({
                url: '/bitrix/services/main/ajax.php?' + $.param(query, true),
                type: 'POST',
                data: formData
            });

            request.done(function (result) {
                var selectedDelivery = deliveryBlock.find('input:checked');
                var selectedDeliveryId = 0;
                if(selectedDelivery.length > 0) {
                    selectedDeliveryId = selectedDelivery.val();
                }

                $.each(result.data, function (i, deliveryData) {
                    $('.delivery_price_id_'+deliveryData.id).html(deliveryData.price_display);
                    if(deliveryData.id == selectedDeliveryId){
                        renderTotal(deliveryData);
                    }
                });

                if(selectedDeliveryId > 0) {
                    deliveryBlock.find('#delivery_' + selectedDeliveryId).prop('checked', true);
                }
            });

            request.fail(function () {
                console.error('Can not get delivery data');
            });
        }
    });

    /** */
    deliveryBlock.change(function (event) {
        var locationCode = $(event.target).val();

        if (locationCode.length > 0) {


            $('.child_delivery').each(function (){
                if(!$(this).hasClass('hidden')){
                    $(this).addClass('hidden');
                }
            });

            $('.child_deliveries .' + $(event.target).data('child')).removeClass('hidden');

            var formData = $('#os-order-form').serialize();

            var query = {
                c: 'opensource:order',
                action: 'calculateDeliveries',
                mode: 'ajax'
            };

            var request = $.ajax({
                url: '/bitrix/services/main/ajax.php?' + $.param(query, true),
                type: 'POST',
                data: formData
            });

            request.done(function (result) {

                var selectedDelivery = deliveryBlock.find('input:checked');
                var selectedDeliveryId = 0;
                if(selectedDelivery.length > 0) {
                    selectedDeliveryId = selectedDelivery.val();
                }

                $.each(result.data, function (i, deliveryData) {
                    if(deliveryData.id == selectedDeliveryId){
                        renderTotal(deliveryData);
                    }
                });

                if(selectedDeliveryId > 0) {
                    deliveryBlock.find('#delivery_' + selectedDeliveryId).prop('checked', true);
                }
            });

            request.fail(function () {
                console.error('Can not get delivery data');
            });

            query = {
                c: 'opensource:order',
                action: 'resetFeilds',
                mode: 'ajax'
            };

            request = $.ajax({
                url: '/bitrix/services/main/ajax.php?' + $.param(query, true),
                type: 'POST',
                data: formData
            });

            let groupID = 3;

            $('.group_id_'+groupID).remove();

            if(!$('.show-sdek-map').hasClass('hidden')){
                $('.show-sdek-map').addClass('hidden');
            }
            if(!$('.group_id_location').hasClass('hidden')){
                $('.group_id_location').addClass('hidden');
            }

            if(locationCode == jsDeliveryParams['FROM_SHOP']){
                $('.pick_yourself').addClass('active');
            }else{
                $('.pick_yourself').removeClass('active');
            }

            request.done(function (result) {
                let total = parseInt($('.total-block .total-val').text());
                $('.dop-price').remove();
                if(result.data.price>total){
                    let discont = parseInt(result.data.price-parseInt($('.total-block .total-val').text()));
                    let html ='<tr class="dop-price"><td class="title">Наценка</td>';
                    html += '<td class="discont"><span class="dop-value">'+discont+'</span> <span class="rub"></span></td>';
                    html += '</tr>';
                    $('.discont-block').before(html);
                }
                $('.total-block .total-val').text(result.data.price);
                $.each(result.data.props, function (i, props) {
                    if(props.PROPS_GROUP_ID == groupID){
                        renderProperty(props);
                    }

                });
            });

            request.fail(function () {
                console.error('Can not get delivery data');
            });

            let city = $('#property_CITY').text().split(',')[0];

            if (IPOLSDEK_pvz.Y_map){
                IPOLSDEK_pvz.chooseCity(city);
            }
        }
    });

    var quantityItems = 0;
    $(".quantity-item").each(function(){
        var text = $(this).html();
        quantityItems = quantityItems + parseInt(text.replace(/\D+/g,""));        
    });
    $(".total-quantity").html(quantityItems);

    $(".delivery-change .delivery").click(function(){
		if(!$(this).hasClass("selected")){
			$(".delivery-change .delivery").each(function(){
				$(this).removeClass("selected");
            });
			$(this).addClass("selected");
		}
    });
    $(".next-step.button").click(function (e) {
		e.preventDefault();
		var parent = $(this).parents(".order-form");
		var successFlag = false;
        var error = false;
        $(".order-form .required input").each(function(){
            if ($(this).attr("id") == "property_CITY-selectized") {
            }else{
                if($(this).val()){
                    $(this).removeClass('error');
                }else{
                    $(this).addClass('error');
                    error = true;
                }
            }
        });

        if ($('.group_id_location').hasClass('hidden')) {
            $('.property_SDEK_ADDR_error').remove();
        }else{
            $('.property_SDEK_ADDR_error').remove();
            if(!$('#property_SDEK_ADDR').val().length){
                $('.delivery-box').prepend('<div class="error property_SDEK_ADDR_error" style="color:red;">Укажите аддрес доставки на карте</div>');
                error = true;
            }
        }

        if(!error){
            parent.removeClass("step-1");
            parent.addClass("step-2");
            successFlag = true;

            let diliveryID = $('.delivery input:checked').val();

            if(!diliveryID){
                diliveryID = $('.child_delivery input:checked').val();
            }

            if(!$('.child_deliveries').hasClass('hidden')){
                $('.child_deliveries').addClass('hidden');
            }

            /** Если доставка без наложенного платежа */
            if(diliveryID == jsDeliveryParams['PVZ_CDEK']){
                $('#pay_system_1+label, #pay_system_1').remove();
                $('#pay_system_4').click();
            }

            /** Если доставка наложенный платеж */
            if(diliveryID == jsDeliveryParams['PVZ_CDEK_1']){
                $('#pay_system_4+label, #pay_system_4').remove();
            }

            scrollTo(parent);
            $(".submit.button").show();
        }else{
            scrollTo(parent); 
        }
		if(successFlag){
			$(".navigation .item").each(function(){
				$(this).removeClass("selected");
			});
			$(".navigation .item").eq(2).addClass("selected");
		}
    });

    $(".os-order .next-step.button").click(function(){
        login($("#property_NAME").val(), $("#property_EMAIL").val());
    });

    function login(name, mail) {

        var query = {
            c: 'degva:login',
            action: 'login',
            mode: 'class'
        };

        var data = {
            name: name,
            mail: mail,
            SITE_ID: 's1',
            sessid: BX.bitrix_sessid()
        };

        var request = $.ajax({
            url: '/bitrix/services/main/ajax.php?' + $.param(query, true),
            method: 'POST',
            data: data
        });

        request.done(function (response) {
            if (response.data.done == true) {
                //Логика
                console.log(response.data.done);
            } else {
                console.log("no change");
            }
        });
    }

    $('.child_deliveries .child_delivery label').on('click', function (event){
        let input = $(this).parent().find('input');
        if(input.is(':checked')){
            event.preventDefault();
            $('#'+input.data('parent')).click();
        }
    });

    $('.order-form .delivery-change .delivery.selected').click();
});
