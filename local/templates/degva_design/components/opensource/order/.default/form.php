<?php

use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Web\Json;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var OpenSourceOrderComponent $component */

$jsDelivery = [];

?>
<div class="basket-top">
	<div class="navigation right-block">
		<a href="/basket/" class="item">Оформление<span class="break-word"><br/></span> заказа</a>
		<a href="/basket/order/" class="item selected">Оформление<span class="break-word"><br/></span> доставки</a>
		<div class="item">Оплата<span class="break-word"><br/></span> заказа</div>	
	</div>
	<div class="left-block"><h1>Оформление доставки</h1></div>
</div>
<form class="order-form step-1" action="" method="post" name="os-order-form" id="os-order-form">

    <input type="hidden" name="person_type_id" value="<?=$arParams['PERSON_TYPE_ID']?>">
    <div id='delivery-variants'>
        <div class="delivery-change">
            <?
            $selected = ' selected';
            $deliveryChilds = [];
            foreach ($arResult['DELIVERY_GROUPS'] as $deliveryType=>$arDelivery){
                $jsDelivery[$deliveryType] = $arDelivery['ID'];
                ?><label  class="delivery type_<?=$deliveryType?><?=$selected?>">
                    <?$selected = '';?>
                    <input type="radio" id="delivery_<?=$arDelivery['ID']?>" data-child="type_<?=$deliveryType?>" name="delivery_id" class="delivery-input" value="<?=$arDelivery['ID']?>">
                    <?= $arDelivery['NAME'] ?>
                    <span class="delivery-information-and-childes-block">
                        <span class="price delivery_price_id_<?=$arDelivery['ID']?>"><?= $arDelivery['PRICE'] ?></span>
                        <?if(!empty($arDelivery['CHILDES'])){
                            foreach ($arDelivery['CHILDES'] as $id => $child){

                                $deliveryChilds[$deliveryType][$id] = [
                                    'PARENT_ID' => $arDelivery['ID'],
                                    'NAME' => $child['NAME'],
                                    'PRICE' => $child['PRICE'],
                                    'DESCRIPTION' => $child['DESCRIPTION'],
                                ];
                            }
                        }?>
                    </span>
                </label><?
            }?>
        </div>
        <? if(!empty($deliveryChilds)){
            ?>
            <div class="child_deliveries">
                <?foreach ($deliveryChilds as $deliveryType=>$childes){
                    $i = 1;
                    ?>
                    <div class="child_delivery type_<?=$deliveryType?> hidden">
                        <?foreach ($childes as $id=>$child){
                            if($deliveryType == 'PVZ_CDEK'){
                                $jsDelivery[$deliveryType.'_'.$i] = $id;
                                $i++;
                            }?>
                            <input id="delivery_child_<?=$id?>" type="radio" data-child="type_<?=$deliveryType?>" data-parent="delivery_<?=$child['PARENT_ID']?>" name="delivery_id" class="hidden" value="<?=$id?>">
                            <label for="delivery_child_<?=$id?>">
                                <span class="child-name"><?=$child['NAME']?> <span class="price delivery_price_id_<?=$id?>"><?=$child['PRICE']?></span>*</span>
                                <div class="child-description"><?=$child['DESCRIPTION']?></div>
                            </label>

                        <?}?>
                    </div>
                <?}?>
            </div>
        <?}?>
    </div>

    <?$agreement = false;?>
    <div class="pick_yourself">ул.Высоковольтная д.33<span class="break-word"><br/></span> (с 10:00 до 19:00 без выходных)</div>
    <div class="delivery-box">
        <h2 class="h3 mobile-hide"><span class="title_addr">Адрес доставки</span><span style="display: none;" class="title_info">Личные данные</span></h2>

        <div class="delivery-properties">
            <?php foreach ($arResult['PROPERTIES'] as $propCode => $arProp) { ?>
                <?if($propCode == "AGREEMENT"){
                    $agreement = '
                    <div class="input-wrapper property_'.$propCode.' required">
                    <input id="'.$arProp["FORM_LABEL"].'" type="checkbox" checked
                    name="'.$arProp["FORM_NAME"].'"
                    value="Y">
                    <label for="'. $arProp["FORM_LABEL"] .'">
                        Подтверждая заказ Вы соглашаетесь на условия 
                        <a href="/upload/offer-doc.pdf" target="_blank">
                        договора оферты
                        </a>
                        <br/>
                        и
                        <a href="/upload/agreement.pdf" target="_blank">
                        политики в отношении обработки персональных данных
                        </a>
                    </label>
                    </div>
                    ';
                }else{?>
                <?if($arProp['TYPE'] != 'LOCATION'){
                    $groupID = 'group_id_'.$arProp['PROPS_GROUP_ID'];
                }else{
                    $groupID = 'group_id_location hidden';
                }
                ?><div class="input-wrapper <?=$groupID?> property_<?=$propCode?><?if($arProp['IS_REQUIRED']){?> required<?}?>">
                    <label for="<?= $arProp['FORM_LABEL'] ?>">
                        <?= $arProp['NAME'] ?>
                        <?php if($arProp['IS_REQUIRED']){?>
                            <span class="required" title="%s">*</span>
                        <?}?>
                    </label>
                    <?php
                    switch ($arProp['TYPE']):
                        case 'LOCATION':
                            ?>
                            <div class="location">
                                <select class="location-search" name="<?= $arProp['FORM_NAME'] ?>"
                                        id="<?= $arProp['FORM_LABEL'] ?>">
                                    <option
                                            data-data='<?echo Json::encode($arProp['LOCATION_DATA'])?>'
                                            value="<?= $arProp['VALUE'] ?>"><?=$arProp['LOCATION_DATA']['label']?></option>
                                </select>
                            </div>
                            <?
                            break;

                        case 'ENUM':
                            foreach ($arProp['OPTIONS'] as $code => $name):?>
                                <label class="enum-option">
                                    <input type="radio" name="<?= $arProp['FORM_NAME'] ?>" value="<?= $code ?>">
                                    <?= $name ?>
                                </label>
                            <?endforeach;
                            break;

                        case 'DATE':
                            $APPLICATION->IncludeComponent(
                                'bitrix:main.calendar',
                                '',
                                [
                                    'SHOW_INPUT' => 'Y',
                                    'FORM_NAME' => 'os-order-form',
                                    'INPUT_NAME' => $arProp['FORM_NAME'],
                                    'INPUT_VALUE' => $arProp['VALUE'],
                                    'SHOW_TIME' => 'Y',
                                    //'HIDE_TIMEBAR' => 'Y',
                                    'INPUT_ADDITIONAL_ATTR' => 'placeholder="выберите дату"'
                                ]
                            );
                            break;

                        case 'Y/N':
                            ?>
                            <input id="<?= $arProp['FORM_LABEL'] ?>" type="checkbox"
                                    name="<?= $arProp['FORM_NAME'] ?>"
                                    value="Y">
                            <?
                            break;

                        default:
                            ?>
                            <input<?if($arProp['CODE']=='SDEK_ADDR'){echo ' hidden';}?> id="<?= $arProp['FORM_LABEL'] ?>" type="text"
                                    name="<?= $arProp['FORM_NAME'] ?>"
                                    value="<?= $arProp['VALUE'] ?>">
                        <? endswitch; ?>
                </div><?
                }?>
            <? } ?>
            <?php if (count($component->errorCollection) > 0): ?>
                <div class="errors_list">
                    <?php foreach ($component->errorCollection as $error):
                        /**
                         * @var Error $error
                         */
                        ?>
                        <div class="error"><?= $error->getMessage() ?></div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="show-sdek-map hidden">
            <h2 class="title">Выберете на карте пункт самовывоза</h2>
            <?$APPLICATION->IncludeComponent(
                "ipol:ipol.sdekPickup",
                "",
                Array(
                    "CITIES" => array(),
                    "CNT_BASKET" => "Y",
                    "CNT_DELIV" => "Y",
                    "COUNTRIES" => array(),
                    "FORBIDDEN" => array("0","courier","inpost"),
                    "MODE" => "PVZ",
                    "NOMAPS" => "N",
                    "PAYER" => "1",
                    "PAYSYSTEM" => "4",
                    "SEARCH_ADDRESS" => "N"
                )
            );?>
        </div>
        <div class="input-wrapper comment">
            <label for="property_USER_DESCRIPTION">Комментарий</label>
            <textarea name="USER_DESCRIPTION" id="property_USER_DESCRIPTION" cols="30" rows="10"></textarea>
        </div>
    </div>

    <div class="payment-block custom-check">
        <h2 class="h3"><?= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_PAY_SYSTEMS_TITLE')?>:</h2>
        <? foreach ($arResult['PAY_SYSTEM_ERRORS'] as $key => $error):
            /** @var Error $error */
            ?>
            <div class="error"><?= $error->getMessage() ?></div>
        <? endforeach;
        foreach ($arResult['PAY_SYSTEM_LIST'] as $key => $arPaySystem): ?>
            <?
            if($key==1){$arPaySystem['CHECKED'] = 'checked';}?>            
                <input id="pay_system_<?= $arPaySystem['ID'] ?>" type="radio" name="pay_system_id"
                    value="<?= $arPaySystem['ID'] ?>"
                    <?= $arPaySystem['CHECKED'] ? 'checked' : '' ?>
                >
                <label for="pay_system_<?= $arPaySystem['ID'] ?>" class="item">
                    <?= $arPaySystem['NAME'] ?>
                </label>
            <br>
        <? endforeach; ?>
        <div class="payment-images">
            <img src="/images/cards.png" alt="" class="img payment-img">
        </div>
        <?echo $agreement;?>
    </div>
    <div class="order-basket scroll-with-page">
        <table class="order-table">
            <tr class="total-block">
                <td class="title">ИТОГО:</td>
                <td class="total"><span class="total-val"><?= $arResult['SUM'] + $delivery?></span> <span class="rub"></span></td>
            </tr>
            <tr>
                <td class="title">Товары, <span class="total-quantity">0</span> шт</td>
                <td class="no-discont"><span class="no-discont-val"><?= $arResult['PRODUCTS_BASE_PRICE'] ?></span> <span class="rub"></span></td>
            </tr>
            <tr>
                <td class="title">Доставка</td>
                <td class="delivery-total"><span class="delivery-value"><?=$delivery?></span> <span class="rub"></span></td>
            </tr>
            <tr class="discont-block">
                <td class="title">Скидка</td>
                <td class="discont"><span class="discont-value"><?= $arResult['PRODUCTS_DISCOUNT'] ?></span> <span class="rub"></span></td>
            </tr>
        </table>
        <h2 class="h3 order-label"><?= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_BASKET_TITLE')?></h2>    
        <?if(CModule::IncludeModule("iblock")){?>
            <? foreach ($arResult['BASKET'] as $arBasketItem): ?>
                <div class="item-wrapper">
                    <?$res = CIBlockElement::GetByID($arBasketItem['PRODUCT_ID']);
                    if($ar_res = $res->GetNext()){
                        $picture = [];
                        if($ar_res["PREVIEW_PICTURE"]){
                            $picture = CFile::ResizeImageGet($ar_res["PREVIEW_PICTURE"], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true, \Stuls\Helper::WMParams());
                        }else{
                            $picture["src"] = "/images/no_photo.png";
                        }
                    }
                    ?>
                    <div class="picture">
                        <img src="<?=$picture["src"]?>" title="Картинка для товара <?= $arBasketItem['NAME'] ?>" alt="Картинка для товара <?= $arBasketItem['NAME'] ?>">
                    </div><!--
                --><div class="item">
                        <div class="name">
                            <?= $arBasketItem['NAME'] ?>
                        </div>
                        <div class="quantity-item"><?= $arBasketItem['QUANTITY_DISPLAY'] ?></div>
                    </div>
                </div>
            <? endforeach; ?>
        <?}?>
        <input type="hidden" name="save" value="y">
        <br>
        <div class="center-block">
            <button class="next-step button">Отправить заказ</button>
            <button class="submit button" type="submit">Отправить заказ</button>
        </div>
    </div>
</form>
<div class="clear"></div>
<script>
    var jsDeliveryParams = <?=json_encode($jsDelivery)?>;
</script>