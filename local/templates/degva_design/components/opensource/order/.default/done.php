<?php

use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Order;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var OpenSourceOrderComponent $component */
$component = &$this->__component;
$order = $component->order;
?>
<style>
    .os-order .sale-paysystem-wrapper{
        border: none;
        text-align: center;
    }
    .os-order .sale-paysystem-yandex-button-item{
        height: 100%;
        display: block;
        background-color: var(--gold-color);
        padding: 10px 30px;
    }
</style>
<h3 class="h1 center-block pay-info"><?= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_ORDER_CREATED', [
    '#ORDER_ID#' => $arResult['ID']
]) ?></h3>
<?
if (!$order->getPaymentCollection()->isEmpty()) {
    /** @var Bitrix\Sale\Payment $payment */
    $payment = $order->getPaymentCollection()->current();
    $checkedPaySystemId = $payment->getPaymentSystemId();
    $service = \Bitrix\Sale\PaySystem\Manager::getObjectById($payment->getPaymentSystemId());
    if ($service) {
        $context = \Bitrix\Main\Application::getInstance()->getContext();
     
        $result = $service->initiatePay($payment, $context->getRequest(), \Bitrix\Sale\PaySystem\BaseServiceHandler::STRING);
         
        if ($result->isSuccess()) {
            echo $result->getTemplate();
        }
    }
}
?>
