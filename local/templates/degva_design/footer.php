
	<?/*$APPLICATION->IncludeComponent(
		"degva:iblock.element.add.form", 
		"footer-form", 
		array(
			"EMAIL_TO" => "sdeminv@yandex.ru",
			"CUSTOM_NAME" => "Оставить запрос",
			"SEF_MODE" => "N",
			"IBLOCK_TYPE" => "Services",
			"IBLOCK_ID" => "13",
			"BASE_IBLOCK_ID" => "13",
			"PROPERTY_CODES" => array(
				0 => "29",
				1 => "30",
				2 => "31",
				3 => "32",
				4 => "37",
				5 => "NAME",
			),
			"PROPERTY_CODES_REQUIRED" => array(
				0 => "29",
				1 => "30",
				2 => "31",
				3 => "32",
				4 => "37",
				5 => "NAME",
			),
			"GROUPS" => array(
				0 => "1",
				1 => "2",
				2 => "3",
				3 => "4",
			),
			"STATUS_NEW" => "N",
			"STATUS" => "ANY",
			"LIST_URL" => "",
			"ELEMENT_ASSOC" => "CREATED_BY",
			"MAX_USER_ENTRIES" => "100000",
			"MAX_LEVELS" => "100000",
			"LEVEL_LAST" => "Y",
			"USE_CAPTCHA" => "N",
			"USER_MESSAGE_EDIT" => "",
			"USER_MESSAGE_ADD" => "",
			"DEFAULT_INPUT_SIZE" => "30",
			"RESIZE_IMAGES" => "N",
			"USE_AGREEMENT" => "N",
			"MAX_FILE_SIZE" => "0",
			"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
			"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
			"CUSTOM_TITLE_NAME" => "Оставить запрос",
			"CUSTOM_TITLE_TITLE" => "Оставить запрос",
			"CUSTOM_TITLE_TAGS" => "",
			"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
			"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
			"CUSTOM_TITLE_IBLOCK_SECTION" => "",
			"CUSTOM_TITLE_PREVIEW_TEXT" => "",
			"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
			"CUSTOM_TITLE_DETAIL_TEXT" => "",
			"CUSTOM_TITLE_DETAIL_PICTURE" => "",
			"SHOW_OBJECTS" => "Y",
			"AJAX_MODE" => "Y",
			"AJAX_OPTION_SHADOW" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"COMPONENT_TEMPLATE" => "footer-form"
		),
		false
	);*/?>
		<?if (!defined('UNIQUE_PAGE') && UNIQUE_PAGE != 'Y') {?>
			</div>
		<?}?>
	</main>
	<footer class="footer">
		<div class="center1200">
			<?if (defined('MAIN_PAGE') && MAIN_PAGE == 'Y') {?>
				<div class="logo">
					<img src="/images/logo.png" title="logo" alt="logo"/>
				</div>
			<?}else{?>
				<a href="/" class="logo">
					<img src="/images/logo.png" title="logo" alt="logo"/>
				</a>
			<?}?>
			<div class="contact-block">
				<a href="tel:+78007079724" class="phone">8 800 707 97 24</a>
				<div class="text">Телефон магазина</div>
			</div>
			<div class="contact-block-wrapper">
				<div class="contact-block">
					<a href="mailto:stulsrzn@stuls.ru" class="mail">stulsrzn@stuls.ru</a>
					<div class="text">Администратор Рязань</div>
				</div>
			</div>
			<div class="footer-menu">
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu", 
					"footer-menu", 
					array(
						"ALLOW_MULTI_SELECT" => "N",
						"CHILD_MENU_TYPE" => "left",
						"COMPONENT_TEMPLATE" => ".default",
						"DELAY" => "N",
						"MAX_LEVEL" => "2",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_TYPE" => "N",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"ROOT_MENU_TYPE" => "top",
						"USE_EXT" => "N"
					),
					false
				);
				?>			
				<?$APPLICATION->IncludeComponent(
					"bitrix:catalog.section.list",
					"footer-menu",
					Array(
						"ADD_SECTIONS_CHAIN" => "Y",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"COUNT_ELEMENTS" => "Y",
						"COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
						"FILTER_NAME" => "sectionsFilter",
						"IBLOCK_ID" => "2",
						"IBLOCK_TYPE" => "Catalog",
						"SECTION_CODE" => "",
						"SECTION_FIELDS" => array("",""),
						"SECTION_ID" => "",
						"SECTION_URL" => "",
						"SECTION_USER_FIELDS" => array("",""),
						"SHOW_PARENT_NAME" => "Y",
						"TOP_DEPTH" => "2",
						"VIEW_MODE" => "LINE"
					)
				);?>
			</div>
			<p class="label"><?=date("Y")?> © Кресла & Стулья</p>
		</div>
	</footer>
</body>
</html>