
$(document).ready(function () {
    ymaps.ready(init);

    function init() {
        var myMap = new ymaps.Map("map", {
                center: [54.619226, 39.700539],
                zoom: 15
            }),

            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                hintContent: 'STULS',
                balloonContent: 'Кресла и стулья<br/>ТЦ "Квазар" 2 этаж<br/>'
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: '/images/svg/map.svg',
                // Размеры метки.
                iconImageSize: [60, 70],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-5, -38]
            });
    
            myMap.geoObjects
            .add(myPlacemark);
    }
    
});