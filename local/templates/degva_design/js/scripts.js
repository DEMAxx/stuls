function validateMail(text) {  
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(text);
}
function validateText(text) {  
	var re = /^[\s\n.,!?+:;()'"«»_a-zA-Z\-0-9а-яёА-ЯЁ\/]{2,}$/;
	return re.test(text);
}
function validateNum(text) {  
	var re = /^[0-9]{1,}$/;
	return re.test(text);
}

function getOffset(elem){
	if(elem.length){
		var offset=Math.round(elem.offset().top);
		return offset;
	}
}
function scrollTo(elem,delta){
	if(!delta) delta=0;
	$("html:not(:animated),body:not(:animated)").animate({scrollTop: getOffset(elem)-delta}, 800);
	return false;
}
function scrollElem(elem,scrollTop,elem2){
	var top=elem.data('top');
	if((!elem.length)||(screen.width < 1024)||(screen.height < elem.height())||(scrollTop<top)){
		elem.css('transform','translateY(0)');
	}else{
		var height=elem.height();
		if(elem2.length)
			var maxScroll=elem2.height();
		else 
			var maxScroll=elem.parent().height();
		if(maxScroll<(height+scrollTop-top+170)) return;
		elem.css('transform','translateY('+(scrollTop-top+130)+'px)');
	}
}

function resizeLikeWindow(obj,delta){
	var winHeight=$(window).height();
	var deltaHeight=delta.length?delta.height():0;
	obj.height(winHeight-deltaHeight+'px');
}

function centerPopup(popupBox){
	var scrolledTop=$(document).scrollTop();
	var winHeight=$(window).height();
	var boxHeight=popupBox.children().height();
	var top=scrolledTop+30;
	if(boxHeight<winHeight) top=scrolledTop+(winHeight-boxHeight)/2;
	popupBox.css({'top':top+'px'});
}
function showMess(str){
	popup=$('#popup');
	popupBox=$('#popup .box');
	popupBox.html(str);
	popup.fadeIn(300);
	centerPopup(popupBox);
	//setInpMask(popupBox);
}
function showTextMess(text,addClass,timeout){
	var str = '<div class="message '+addClass+'">';
	str += '<div class="close"></div>';
	str += text;
	str += '</div>';
	showMess(str);
	if(timeout>0) {//to remove message after timeout
		setTimeout(function () {
			$('.popup-shadow').click();
		}, timeout);
	}
}
var owlCarouselReady = false; 
function setGallery(targetClass, items, center=true){
	$(targetClass).owlCarousel({
		loop:true,
		margin:15,
		nav:true,
		dots:true,
		navText: [' ',' '],
		items:1,
		slideBy:1,
		center: center,
		responsive:{
			600 :{items:2},
			960 :{
				items:items,
				center:false,
				margin:20,
			},
		},
		onInitialized: counter,
		onChanged: counter,
	});
}
function counter(event) {
	if(event.currentTarget.classList[1] == "gallery-share"){
		if (!event.namespace) {
			return;
		}
		var slides = event.relatedTarget;
		$('.counter.c-share').html('<span class="num" style="width:' + (slides.relative(slides.current()) + 1)/slides.items().length*100 + '%;"></span>');
	}
}
function counterHit(event) {
	if(event.currentTarget.classList[1] == "gallery-hit"){
		if (!event.namespace) {
			return;
		}
		var slides = event.relatedTarget;
		$('.counter.hit').html('<span class="num" style="width:' + (slides.relative(slides.current()) + 1)/slides.items().length*100 + '%;"></span>');
	}
}
function counterNew(event) {
	if(event.currentTarget.classList[1] == "gallery-new"){
		if (!event.namespace) {
			return;
		}
		var slides = event.relatedTarget;
		$('.counter.new').html('<span class="num" style="width:' + (slides.relative(slides.current()) + 1)/slides.items().length*100 + '%;"></span>');
	}
}

function setCalendar(box){
	var inpDate=$(box);
	if(inpDate.length){
		inpDate.each(function(){
			var calendar=$(this).find("input");
			var mkRemoveDates = (calendar.data('reset'))?calendar.data('reset').split(";"):[];//даты которые нужно блокировать в календаре
			var mkDateFrom=calendar.data('mkfrom')+'000';
			var mkDateTo=calendar.data('mkto')+'000';
			$(this).mask("99.99.9999",{autoclear: false});
			$(this).pickmeup({
				date:calendar.val(),
				flat:true,
				position:'bottom',
				class_name:'datepicker',
				hide_on_select:true,
				format:'d.m.Y',
				min:calendar.data('min'),
				max:calendar.data('max'),
				select_month: false,
				select_year: false,
				change: function (date) {setInpDate(date);return false;},
				locale : {
					days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
					daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
					daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
					months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
					monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
				},
				render: function(date) {
					var mkDate=date.valueOf()+'';
					var disabled=false;
					if( mkRemoveDates.indexOf(mkDate) != -1 ||
							mkDateFrom > mkDate ||
							mkDateTo < mkDate ){
						disabled=true;
					}
					return {
						disabled: disabled
					}
				}
			});

		})
	}
}
function setBasketCount(){
	var itemCount = 0;
	$(".basket-item-amount-filed").each(function(){
		itemCount = Number.parseInt(itemCount) + Number.parseInt($(this).val());
	});
	$(".basket-item-count .count-num").html(itemCount);
}
function setInpDate(date){
	var string='';
	var old_string=$('.calendar-inp').val();
	for(var i=0;i<date.length;i++){
		string +=date[i];
	}
	if(string!=old_string){//изменяем значение только при несовпадении
		$('.calendar-inp').val(string);
		//$('form.filt-form').submit();
	}
	
	//удаляем дни недели,если не соответсвуют выбранным дням
	var days=$('.calendar').pickmeup('get_date', 'w');//отключаем всвязи с аяксом по каждому клику,проверку переносим на php
	var days_min={};
	var week_days={};
	var flag=false;
	for(var i=0;i<days.length;i++){
		days_min[days[i]]=days[i];
	}
	$('#week-day input:checked').each(function(){
		var num=$(this).data('num');
		week_days[num]=num;
	})
	for(var e in days_min){
		if(e!=week_days[e]){
			$('#week-day input:checked').removeAttr("checked");
			break;
		}
	}
	
	/*for(var e in days){
		if(e == x) return;
	}
	alert('Такого элемента не существует'); */
	
};
function changer(id, iblock, elem){
	var file = '/ajax/project_main.php';
	area = elem.parents(".changer");
	$.ajax({
		url: file,
		data: {id: id, iblock: iblock},
		method: 'POST',
		success: function(data){
			$(area).html(data);
			if($(".gallery-about-us").length){setGallery(".gallery-about-us", 1);}
		}
	});
	return false;
}
$(document).ready(function () {

	var popup=$('#popup');
	var popupBox=$('#popup .wrapper');
	var navItems=$('#nav .item');
	var scroll=$(window).scrollTop();
	var $win=$(window);
	var winHeight=$(window).height();
	var header=$('#header');
	var $body=$('body');
	var $fixItem = $(".scroll-with-page");
	var scrolledTop=0;
	var menu_offset=50; 

	popup.on('click','.ok, .cross, .close, .popup-shadow',function(){
		popup.fadeOut(300,function(){
			setTimeout(function(){popup.html();},500);
		});
	});

	if($(window.location.hash).length){
		navItems.filter('[href="'+window.location.hash+'"]').click();
	}

	if($fixItem.length){
		var $fixItemTo = $($fixItem.data('fix-to')).first();
		$fixItem.data('top',$fixItem.offset().top);
	}

	if(scrolledTop>=menu_offset){
		page_scrolled=true;
	}else{
		page_scrolled=false;
	}
	$(window).scroll(function(){		
		setTimeout(function(){
			var scrolledTop=$(window).scrollTop();
			if((scrolledTop>=menu_offset)&&(!page_scrolled)){
				page_scrolled=true;
				$body.addClass('scrolled');
			}else if((scrolledTop<menu_offset)&&(page_scrolled)){
				page_scrolled=false;
				$body.removeClass('scrolled');
			}
			if($fixItem.length) scrollElem($fixItem,scrolledTop,$fixItemTo);
		},100);
	});
	$(window).resize(function(){
		if($fixItem.length) $fixItem.data('top',$fixItem.offset().top);
	});




	$(".input.mail").each(function(){
		$(this).on("change", function(){
			var val=$(this).val();
			if(validateMail(val)){
				$(this).parents(".input-box").addClass("success");
				$(this).parents(".input-box").removeClass("error");
			}else{
				$(this).parents(".input-box").addClass("error");
				$(this).parents(".input-box").removeClass("success");
			}
		});
	});
	$(".property_EMAIL input").each(function(){
		$(this).on("change", function(){
			var val=$(this).val();
			if(validateMail(val)){
				$(this).parents(".input-box").addClass("success");
				$(this).parents(".input-box").removeClass("error");
			}else{
				$(this).parents(".input-box").addClass("error");
				$(this).parents(".input-box").removeClass("success");
			}
		});
	});
	$(".input.name").each(function(){
		$(this).on("change", function(){
			var val=$(this).val();
			if(validateText(val)){
				$(this).parents(".input-box").addClass("success");
				$(this).parents(".input-box").removeClass("error");
			}else{
				$(this).parents(".input-box").addClass("error");
				$(this).parents(".input-box").removeClass("success");
			}
		});
	});
	$(".input.phone").each(function(){
		$(this).mask("+7(999) 999-99-99");
	});
	$(".property_PHONE input").each(function(){
		$(this).mask("+7(999) 999-99-99");
	});
	/*mobile menu*/
	$(".mobile-menu, .menu-mobile .close").click(function () {
		if($(this).hasClass("close")){
			$(".mobile-menu").removeClass("active");
		}else{
			$(this).addClass("active");
		}
	});
	/*mobile menu end*/
	/*FORMS*//*
	$('.to-buy').click(function(){
		$.ajax({
			type: "POST",
			url:"/ajax/buy.php",
			success: function(result){
				showMess(result);
			}
		});
		return false;
	});*/
	/*FORMS END*/

	$('#nav a, a.calc_cost, .arrows, a.button').click(function(){
		var id=$(this).attr('href');
		scrollTo($(id),72);
		return false;
	});


	/*slider end*/
	$('.change-btn').on('touchstart mousedown',function(e){
		var number = 4;
		var btn=$(this);
		var line=$(this).parents('.value-ind');
		var min = parseInt($(this).parents(".slider-box").find(".num.min").html());
		var max = parseInt($(this).parents(".slider-box").find(".num.max").html());
		var delta = max-min;
		var input=$(this).parents('.amount').find(".amount-value");
		var box=$(this).parents('.slider');
		var boxPosX=box.offset().left;
		var boxWidth=box.width();
		var pageX,percentX;
		//console.log(e);
		box.on('touchmove mousemove',function(e){
			if(e.type=='mousemove') pageX=e.pageX;
			else pageX=event.changedTouches[0].pageX;
			if(!pageX || pageX<boxPosX) pageX=boxPosX;
			if(pageX>(boxPosX+boxWidth)) pageX=(boxPosX+boxWidth);
			percentX=(pageX-boxPosX)*100/boxWidth;
			number = Math.round(percentX*delta/100+4);
			input.val(number);
			$(".value", line).html(number);
			line.css('left',percentX+'%');
		});
		$win.on('touchend mouseup',function(e){
			//console.log(e);
			box.off('touchmove mousemove');
			btn.off('touchend mouseup');
		});
	});

	/*gallery*/
	if($(".gallery-banners").length){
		$(".gallery-banners").owlCarousel({
			loop:true,
			margin:50,
			nav:true,
			dots:false,
			navText: [' ',' '],
			items:1,
			slideBy:1,
			center: true,
			autoplay:true,
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			responsive:{960 :{nav:false},},
		});		

	} 
	if($(".gallery-share").length){setGallery(".gallery-share", 4);}
	if($(".gallery-hit").length){
		$(".gallery-hit").owlCarousel({
			loop:true,
			margin:15,
			nav:true,
			dots:true,
			navText: [' ',' '],
			items:1,
			slideBy:1,
			center: true,
			responsive:{
				600 :{items:2},
				960 :{
					items:4,
					center:false,
					margin:20,
				},
			},
			onInitialized: counterHit,
			onChanged: counterHit,
		});
	}
	if($(".gallery-new").length){
		$(".gallery-new").owlCarousel({
			loop:true,
			margin:15,
			nav:true,
			dots:true,
			navText: [' ',' '],
			items:1,
			slideBy:1,
			center: true,
			responsive:{
				600 :{items:2},
				960 :{
					items:4,
					center:false,
					margin:20,
				},
			},
			onInitialized: counterNew,
			onChanged: counterNew,
		});
	}
	if($(".product-gallery").length){
		$(".product-gallery").slick({
			slidesToShow: 5,  
			slidesToScroll: 1, 
			dots: true,
			vertical: true,
			infinite: false,
			prevArrow: '<div class="prev"></div>',
			nextArrow: '<div class="next"></div>',
			responsive: [
				{
				  breakpoint: 600,
				  settings: {
					slidesToShow: 1,
					vertical: false,
					infinite: true,
				  }
				},
				{
				  breakpoint: 960,
				  settings: {
					slidesToShow: 4,
					infinite: true,
					vertical: true
				  }
				}
			  ]
		
		});
	}
	if($(".gallery-reviews").length){setGallery(".gallery-reviews", 4,false);}
	$(".reviews-block .shower").click(function(){
		$(this).addClass("active");
	});
	$(document).on('click', ".catalog-page .switcher .filter-show", function(){
		$(this).parents(".catalog-page").addClass("active");
	});
	$(document).on('click', ".catalog-page .sidebar-block .close", function(){
		$(this).parents(".catalog-page").removeClass("active");
	});
	
	$(document).on('change', '.sorter .items input', function(e){
		e.preventDefault();
		var val = $(this).val();
	    const url = new URL(window.location);  // == window.location.href
		url.searchParams.set('SORT', val); 
		history.pushState(null, null, url); 
		document.location.href = url;
	});
	$(".product-detail .items").each(function(){
		$(this).click(function(event){
			if($(this).hasClass("active")){
				//if (event.target.classList.contains("product-item-detail-info-container-title")) {
					$(this).toggleClass("active");
				//}
			}else{
				$(this).toggleClass("active");
			}
		});
	});
	/*Свойства торговых предложений*/
	var prop = $(".product-detail .articul").find(".offer");
	$(".prod-short-info .offer").each(function(){
		$(this).remove();
	});		
	$(".prod-short-info .props").append(prop);
	$(document).on('click', ".product-detail .items .product-item-scu-item-text-block", function(){
		var prop = $(".product-detail .articul").find(".offer");
		$(".prod-short-info .offer").each(function(){
			$(this).remove();
		});		
		$(".prod-short-info .props").append(prop);
	});
	/**/
	$(".share-block .switcher .item").click(function(){
		$(".gallery-hit").owlCarousel({
			loop:true,
			margin:15,
			nav:true,
			dots:true,
			navText: [' ',''],
			items:1,
			slideBy:1,
			center: true,
			responsive:{
				600 :{items:2},
				960 :{
					items:4,
					center:false,
					margin:20,
				},
			},
			onInitialized: counterHit,
			onChanged: counterHit,
		});
		$(".gallery-new").owlCarousel({
			loop:true,
			margin:15,
			nav:true,
			dots:true,
			navText: [' ',''],
			items:1,
			slideBy:1,
			center: true,
			responsive:{
				600 :{items:2},
				960 :{
					items:4,
					center:false,
					margin:20,
				},
			},
			onInitialized: counterNew,
			onChanged: counterNew,
		});
		if($(this).hasClass("h3")){
			$(".share-block .switcher .item.h1").addClass("h3");
			$(".share-block .switcher .item.h1").removeClass("h1");
			$(this).addClass("h1");
			$(this).removeClass("h3");
			$(".share-block .hit-sallers").toggleClass("active");
			$(".share-block .new-sallers").toggleClass("active");
		}
	});
	$(".product-detail .product-gallery .img-box").click(function(){
		var pic = $(this).html();
		$(".product-detail .product-item-detail-slider-block .product-item-detail-slider-image.active").html(pic).addClass("custom");
	});
	var imageSrc="";
	var elem_width = 0;
	var elem_height = 0;
	var elem_left = 0;
	var elem_top = 0;
	$(document).on("mouseenter", ".custom img", function() {
		imageSrc = $(".product-item-detail-slider-image img").attr("src");
		var pos = $(this).offset();
		elem_left = pos.left;
		elem_top = pos.top;
		elem_width = $(this).height()/100;
		elem_height = $(this).width()/100;
		 // вывод результата в консоль
		//.position().top;
		//var pos = 
		$(".product-item-detail-slider-image img").css({"background-image":"url("+imageSrc+")",
		"background-size":"200%", 
		"background-position":"center",
		"width":"100%",
		"height":"100%",
		});
		$(".product-item-detail-slider-image img").attr({"src":""});
	});
	$(document).on("mousemove", ".custom img", function(e){
		// положение курсора внутри элемента
		if(e.pageX>0){
			Xinner = (e.pageX - elem_left)/elem_width;
		}
		if(e.pageY>0){
			Yinner = (e.pageY - elem_top)/elem_height;		
		}
		if(Yinner>100){
			Yinner = 100;
		}
		if(Xinner>100){
			Xinner = 100;
		}
		$(".product-item-detail-slider-image img").css({"background-position":Xinner+"% "+Yinner+"%"});
	});
	$(document).on("mouseleave", ".custom", function() {
		$(".product-item-detail-slider-image img").attr({"src":imageSrc});
		$(".product-item-detail-slider-image img").css({"background-size":"100%",
		 "background-position":"center",
		 "background-image":"",
		 "width":"auto",
		 "height":"auto",
		});
	});
	if($("#basket-root")){
		setBasketCount();
		$("#basket-root").click(function(){
			setTimeout(function(){
				setBasketCount();
			},1000);
		});
	}
	
    $(".reviews-page .show-all").click(function(){
        $(".reviews-list .review").each(function(){
			$(this).addClass("active");			
		});   
		$(this).hide();  
	}); 
	$('.button.set-review').click(function(){
		var id = $(this).data("id");
		$.ajax({
			url: "/includes/loadform.php",
			data: {id: id},
			method: 'POST',  
			success: function(data){
				showTextMess(data,'request-form');
				$(".input.mail").each(function(){
					$(this).on("change", function(){
						var val=$(this).val();
						if(validateMail(val)){
							$(this).parents(".item.required").addClass("success");
							$(this).parents(".item.required").removeClass("error");
						}else{
							$(this).parents(".item.required").addClass("error");
							$(this).parents(".item.required").removeClass("success");
						}
					});
				});
				$(".input.name").each(function(){
					$(this).on("change", function(){
						var val=$(this).val();
						if(validateText(val)){
							$(this).parents(".item.required").addClass("success");
							$(this).parents(".item.required").removeClass("error");
						}else{
							$(this).parents(".item.required").addClass("error");
							$(this).parents(".item.required").removeClass("success");
						}
					});
				});
				$(".input.phone").each(function(){
					$(this).mask("+7(999) 999-99-99");
				});
				$('.star').hover(function(){
					var rating = $(this).data('rating');
					var block=$(this).parents('.rating-line');
					$('.star',block).each(function(key,val){
						if(key<rating){
							$(this).addClass("on");
						}
					});
				},function(){
					var rating = $(this).data('rating');
					var block=$(this).parents('.rating-line');
					$('.star',block).each(function(key,val){
						if(key<rating){
							$(this).removeClass("on");
						}
					});
				});
				$('.star').click(function(){
					var block=$(this).parents('.inp-box');
					var blockLine=$(this).parents('.rating-line');
					var vote=$(this).data('rating');
					$('span',block).each(function(){
						var voteText = $(this).data('rating');
						if(voteText == vote){
							$(this).removeClass('hidden');
						}else{
							$(this).addClass('hidden');
						}
					});
					$('.star',blockLine).each(function(key,val){
						if(key<vote){
							$(this).addClass("active");
						}else{
							$(this).removeClass("active");
						}
					});
				});
			}
		});
	});
	/*gallery end*/
	/*changer()*/
	setCalendar($(".calendar"));
});