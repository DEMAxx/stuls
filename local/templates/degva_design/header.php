<!DOCTYPE html>
<?
global $USER;
global $APPLICATION;

use Bitrix\Main\Page\Asset,
    Bitrix\Main\Config\Option;
?>
<? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/style.css"); ?>
<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/jquery-3.3.1.min.js'); ?>
<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/inputmask.js'); ?>
<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/owl.carousel.min.js'); ?>
<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/jquery.maskedinput.min.js'); ?>
<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/scripts.js'); ?>
<? Asset::getInstance()->addString('<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />'); ?>
<? Asset::getInstance()->addString(
    '<link href="'.$APPLICATION->GetCurDir().'" rel="canonical" type="image/x-icon" />'
); ?>
<? $APPLICATION->ShowHead(); ?>
<html lang="<?=LANGUAGE_ID;?>">
<head>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=10.0"/>
    <?
    $APPLICATION->ShowProperty('MetaOG');
    $APPLICATION->ShowProperty('BeforeHeadClose'); ?>
</head>
<body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(54699550, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/54699550" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
<svg class="svg-clip-paths" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
     width="0" height="0">
    <defs>
        <g id="star">
            <path d="M475.075,186.573c0-7.043-5.328-11.42-15.992-13.135L315.766,152.6L251.529,22.694c-3.614-7.804-8.281-11.704-13.99-11.704
				c-5.708,0-10.372,3.9-13.989,11.704L159.31,152.6L15.986,173.438C5.33,175.153,0,179.53,0,186.573c0,3.999,2.38,8.567,7.139,13.706
				l103.924,101.068L86.51,444.096c-0.381,2.666-0.57,4.575-0.57,5.712c0,3.997,0.998,7.374,2.996,10.136
				c1.997,2.766,4.993,4.142,8.992,4.142c3.428,0,7.233-1.137,11.42-3.423l128.188-67.386l128.197,67.386
				c4.004,2.286,7.81,3.423,11.416,3.423c3.819,0,6.715-1.376,8.713-4.142c1.992-2.758,2.991-6.139,2.991-10.136
				c0-2.471-0.096-4.374-0.287-5.712l-24.555-142.749l103.637-101.068C472.604,195.33,475.075,190.76,475.075,186.573z"/>
        </g>
        <linearGradient id="pinkblue" x1="0" y1="0" x2="100%" y2="100%">
            <stop offset="0%" stop-color="#d3357c"/>
            <stop offset="100%" stop-color="#0d1e9e"/>
        </linearGradient>
    </defs>
</svg>
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<div id="popup" class="popup">
    <div class="box"></div>
    <div id="popup_shadow" class="popup-shadow"></div>
</div>
<div class="callback-messenger">
    <a class="callback-messenger__link" target="_blank" href="https://api.whatsapp.com/send?phone=+79156280559&text=Добрый%20день%20!">
        <svg style="pointer-events:none; display:block; height:40px; width:40px;" width="40px" height="40px" viewBox="0 0 1219.547 1225.016">
            <path fill="#E0E0E0" d="M1041.858 178.02C927.206 63.289 774.753.07 612.325 0 277.617 0 5.232 272.298 5.098 606.991c-.039 106.986 27.915 211.42 81.048 303.476L0 1225.016l321.898-84.406c88.689 48.368 188.547 73.855 290.166 73.896h.258.003c334.654 0 607.08-272.346 607.222-607.023.056-162.208-63.052-314.724-177.689-429.463zm-429.533 933.963h-.197c-90.578-.048-179.402-24.366-256.878-70.339l-18.438-10.93-191.021 50.083 51-186.176-12.013-19.087c-50.525-80.336-77.198-173.175-77.16-268.504.111-278.186 226.507-504.503 504.898-504.503 134.812.056 261.519 52.604 356.814 147.965 95.289 95.36 147.728 222.128 147.688 356.948-.118 278.195-226.522 504.543-504.693 504.543z"></path>
            <linearGradient id="htwaicona-chat" gradientUnits="userSpaceOnUse" x1="609.77" y1="1190.114" x2="609.77" y2="21.084">
                <stop offset="0" stop-color="#5b4323"></stop>
                <stop offset="1" stop-color="#5b4323"></stop>
            </linearGradient>
            <path fill="url(#htwaicona-chat)" d="M27.875 1190.114l82.211-300.18c-50.719-87.852-77.391-187.523-77.359-289.602.133-319.398 260.078-579.25 579.469-579.25 155.016.07 300.508 60.398 409.898 169.891 109.414 109.492 169.633 255.031 169.57 409.812-.133 319.406-260.094 579.281-579.445 579.281-.023 0 .016 0 0 0h-.258c-96.977-.031-192.266-24.375-276.898-70.5l-307.188 80.548z"></path>
            <image overflow="visible" opacity=".08" width="682" height="639" transform="translate(270.984 291.372)"></image>
            <path fill-rule="evenodd" clip-rule="evenodd" fill="#FFF" d="M462.273 349.294c-11.234-24.977-23.062-25.477-33.75-25.914-8.742-.375-18.75-.352-28.742-.352-10 0-26.25 3.758-39.992 18.766-13.75 15.008-52.5 51.289-52.5 125.078 0 73.797 53.75 145.102 61.242 155.117 7.5 10 103.758 166.266 256.203 226.383 126.695 49.961 152.477 40.023 179.977 37.523s88.734-36.273 101.234-71.297c12.5-35.016 12.5-65.031 8.75-71.305-3.75-6.25-13.75-10-28.75-17.5s-88.734-43.789-102.484-48.789-23.75-7.5-33.75 7.516c-10 15-38.727 48.773-47.477 58.773-8.75 10.023-17.5 11.273-32.5 3.773-15-7.523-63.305-23.344-120.609-74.438-44.586-39.75-74.688-88.844-83.438-103.859-8.75-15-.938-23.125 6.586-30.602 6.734-6.719 15-17.508 22.5-26.266 7.484-8.758 9.984-15.008 14.984-25.008 5-10.016 2.5-18.773-1.25-26.273s-32.898-81.67-46.234-111.326z"></path>
            <path fill="#FFF" d="M1036.898 176.091C923.562 62.677 772.859.185 612.297.114 281.43.114 12.172 269.286 12.039 600.137 12 705.896 39.633 809.13 92.156 900.13L7 1211.067l318.203-83.438c87.672 47.812 186.383 73.008 286.836 73.047h.255.003c330.812 0 600.109-269.219 600.25-600.055.055-160.343-62.328-311.108-175.649-424.53zm-424.601 923.242h-.195c-89.539-.047-177.344-24.086-253.93-69.531l-18.227-10.805-188.828 49.508 50.414-184.039-11.875-18.867c-49.945-79.414-76.312-171.188-76.273-265.422.109-274.992 223.906-498.711 499.102-498.711 133.266.055 258.516 52 352.719 146.266 94.195 94.266 146.031 219.578 145.992 352.852-.118 274.999-223.923 498.749-498.899 498.749z"></path>
        </svg>
    </a>
    <div class="callback-messenger__animation"></div>
</div>
<main class="center1920">
    <header class="header">
        <div class="mobile-menu mobile"><span></span></div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "mobile-menu",
            array(
                "ALLOW_MULTI_SELECT"    => "N",
                "CHILD_MENU_TYPE"       => "left",
                "COMPONENT_TEMPLATE"    => ".default",
                "DELAY"                 => "N",
                "MAX_LEVEL"             => "2",
                "MENU_CACHE_GET_VARS"   => array(),
                "MENU_CACHE_TIME"       => "3600",
                "MENU_CACHE_TYPE"       => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE"        => "top",
                "USE_EXT"               => "N",
            ),
            false
        ); ?>
        <div class="top-line center1200">
            <div class="left-side with-menu">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "menu",
                    array(
                        "ALLOW_MULTI_SELECT"    => "N",
                        "CHILD_MENU_TYPE"       => "left",
                        "COMPONENT_TEMPLATE"    => "menu",
                        "DELAY"                 => "N",
                        "MAX_LEVEL"             => "2",
                        "MENU_CACHE_GET_VARS"   => array(),
                        "MENU_CACHE_TIME"       => "3600",
                        "MENU_CACHE_TYPE"       => "N",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "ROOT_MENU_TYPE"        => "top",
                        "USE_EXT"               => "N",
                    ),
                    false
                ); ?>
            </div>
            <div class="right-side">
                <? $mail = Option::get('main', 'email_from') ?>
                <a class="mail" href="mailto:<?=$mail?>"><?=$mail?></a>
            </div>
            <div class="clear"></div>
        </div>
        <hr/>
        <div class="bottom-line center1200">
            <a href="/" class="logo">
                <img src="/images/logo.png" title="logo" alt="logo"/>
            </a>
            <div id="basket_1" class="custom-basket">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:sale.basket.basket.line",
                    "desktop",
                    array(
                        "HIDE_ON_BASKET_PAGES" => "Y",
                        "PATH_TO_BASKET"       => SITE_DIR."basket/",
                        "PATH_TO_ORDER"        => SITE_DIR."basket/",
                        "PATH_TO_PERSONAL"     => SITE_DIR."personal/",
                        "PATH_TO_PROFILE"      => SITE_DIR."personal/",
                        "PATH_TO_REGISTER"     => SITE_DIR."login/",
                        "POSITION_FIXED"       => "N",
                        "POSITION_HORIZONTAL"  => "right",
                        "POSITION_VERTICAL"    => "top",
                        "SHOW_AUTHOR"          => "N",
                        "SHOW_DELAY"           => "N",
                        "SHOW_EMPTY_VALUES"    => "Y",
                        "SHOW_IMAGE"           => "Y",
                        "SHOW_NOTAVAIL"        => "N",
                        "SHOW_NUM_PRODUCTS"    => "Y",
                        "SHOW_PERSONAL_LINK"   => "N",
                        "SHOW_PRICE"           => "Y",
                        "SHOW_PRODUCTS"        => "Y",
                        "SHOW_SUMMARY"         => "Y",
                        "SHOW_TOTAL_PRICE"     => "Y",
                    )
                ); ?>
            </div>
            <div class="header__contacts_mobile">
                <div class="header__contact_mobile header__contact_mobile_phone">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_TEMPLATE_PATH."/includes/phones.php"
                        )
                    );?></div>
                <div class="header__contact_mobile header__contact_mobile_address">Рязань, ул. Высоковольтная, д.33</div>
                <div class="header__contact_mobile header__contact_mobile_time">Ежедневно с 10:00 до 19:00</div>
            </div>
            <div class="search">
                <div class="mobile-btn"></div>
                <form action="/catalog/" method="get" class="form">
                    <input class="search-input" type="text" name="q" placeholder="Поиск по сайту">
                    <input type="submit" id="submit_search">
                    <label for="submit_search"></label>
                </form>
            </div>
            <div class="phones-block desktop_show">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_TEMPLATE_PATH."/includes/phones.php"
                    )
                );?>
                <div class="description">Ежедневно с 10:00 до 19:00</div>
            </div>
        </div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "menu",
            array(
                "ADD_SECTIONS_CHAIN"    => "Y",
                "CACHE_FILTER"          => "N",
                "CACHE_GROUPS"          => "Y",
                "CACHE_TIME"            => "36000000",
                "CACHE_TYPE"            => "A",
                "COUNT_ELEMENTS"        => "Y",
                "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
                "FILTER_NAME"           => "sectionsFilter",
                "IBLOCK_ID"             => "2",
                "IBLOCK_TYPE"           => "Catalog",
                "SECTION_CODE"          => "",
                "SECTION_FIELDS"        => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_ID"            => "",
                "SECTION_URL"           => "",
                "SECTION_USER_FIELDS"   => array(
                    0 => "",
                    1 => "",
                ),
                "SHOW_PARENT_NAME"      => "Y",
                "TOP_DEPTH"             => "2",
                "VIEW_MODE"             => "LINE",
                "COMPONENT_TEMPLATE"    => "menu",
            ),
            false
        ); ?>
    </header>
    <? if (!defined('CATALOG_PAGE') && CATALOG_PAGE != 'Y') { ?>
    <? if (!defined('UNIQUE_PAGE') && UNIQUE_PAGE != 'Y') { ?>
    <div class="center1200">
        <h1 class="h1 other-page"><? $APPLICATION->ShowTitle(false) ?></h1>
        <? } ?>
        <? if (!defined('MAIN_PAGE') && MAIN_PAGE != 'Y') { ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "breadcrumb",
                array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => SITE_ID,
                )
            );
            ?>
        <? } ?>
        <? } ?>



