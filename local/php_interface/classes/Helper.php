<?php

namespace Stuls;

use Bitrix\Highloadblock\HighloadBlockTable,
    Bitrix\Main\Loader,
    Bitrix\Main\Application;

class Helper
{
    /**
     * Получаем HL_BLOCK.
     * @param $code
     * @return mixed
     */
    public static function initHlbClass($code)
    {
        Loader::includeModule('highloadblock');
        $params = [
            'cache' => ['ttl' => 86400],
            'limit' => 1,
        ];
        $params['filter'] = ["=NAME" => $code];
        /* Ищем по NAME */
        if ($arData = HighloadBlockTable::getList($params)->fetch()) {
            $arData = HighloadBlockTable::getList($params)->fetch();
            $storeTable = HighloadBlockTable::getById($arData['ID'])->fetch();

            return HighloadBlockTable::compileEntity($storeTable)->getDataClass();
        } else {
            /* Если не находим по NAME ищем по TABLE_NAME */
            $params['filter'] = ["=TABLE_NAME" => $code];
            if ($arData = HighloadBlockTable::getList($params)->fetch()) {
                $arData = HighloadBlockTable::getList($params)->fetch();
                $storeTable = HighloadBlockTable::getById($arData['ID'])->fetch();

                return HighloadBlockTable::compileEntity($storeTable)->getDataClass();
            } else {
                /* Если не находим по TABLE_NAME ищем коду, который был передан в метод */
                $params['filter'] = $code;
                if ($arData = HighloadBlockTable::getList($params)->fetch()) {
                    $arData = HighloadBlockTable::getList($params)->fetch();
                    $storeTable = HighloadBlockTable::getById($arData['ID'])->fetch();

                    return HighloadBlockTable::compileEntity($storeTable)->getDataClass();
                }
            }
        }

    }

    /**
     * @param $days
     * @param string $langID
     * @return string
     */
    public static function getSutokWord($days, $langID = 'ru')
    {
        switch ($days % 100) {
            case 11:
                if ($langID == 'en') {
                    return 'days';
                }

                return 'суток';
                break;
        }
        switch ($days % 10) {
            case 1:
                if ($langID == 'en') {
                    return 'day';
                }

                return 'сутки';
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 0:
                if ($langID == 'en') {
                    return 'days';
                }

                return 'суток';
                break;
        }

        if ($langID == 'en') {
            return 'days';
        }

        return 'суток';
    }

    /**
     * @param $code
     */
    public static function pre($code, $die = false, $all = false)
    {
        global $USER;
        if ($USER->IsAdmin() || ($all == true)) {
            ?>
            <font style="text-align: left; font-size: 12px; color: #555555;">
                <pre><? var_dump($code) ?></pre>
            </font><br>
            <?
        }
        if($die){
            die;
        }
    }

    /**
     * @param $price
     * @param false $check
     * @return string
     */
    public static function formatPrice($price, $check = false)
    {
        if ($check) {
            $format = number_format($price, 0, '.', ' ');
        } else {
            $format = number_format($price, 2, '.', ' ');
        }

        return $format.' ₽';
    }

    /**
     * Переводит в статус оплачено при подтверждении холдированного платежа для печати чека
     * @param $id
     * @param $val
     */
    public static function HolderPayStatusChanger($id,$val){
        if($val=="Y" && \CModule::IncludeModule('sale') && ($arOrder = \CSaleOrder::GetByID($id))) {
            if(in_array($arOrder['STATUS_ID'], array("W"))){//если на стадии "Ожидает подтверждения"
                \CSaleOrder::StatusOrder($id, "P"); //переводим в статус "оплачен, формируется к отправке"
            }elseif(in_array($arOrder['STATUS_ID'], array("N"))){
                \CSaleOrder::StatusOrder($id, "W"); //переводим в статус "Ожидает подтверждения"
            }
        }else{
            \CSaleOrder::StatusOrder($id, "F");
        }
    }

    /**
     * Вернуть параметры для наложения водяного знака
     * @return array
     */
    public static function WMParams(){

        $filePath = Application::getDocumentRoot().SITE_TEMPLATE_PATH.'/images/watermark.png';

        $arFilters = Array(
            array("name" => "watermark", "position" => "tl", "file"=>$filePath, 'fill'=>'resize', 'coefficient'=>'1')
        );

        return $arFilters;

    }

    //todo use Bitrix\Main\Grid\Declension;
    public static function num_word($value, $words, $show = true)

    {

        $num = $value % 100;

        if ($num > 19) {
            $num = $num % 10;
        }

        $out = ($show) ?  $value . ' ' : '';

        switch ($num) {
            case 1:  $out .= $words[0]; break;
            case 2:
            case 3:
            case 4:  $out .= $words[1]; break;
            default: $out .= $words[2]; break;
        }

        return $out;

    }
}