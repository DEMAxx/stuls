<?php

use Bitrix\Main\EventManager;

EventManager::getInstance()->addEventHandler(
    "sale",
    "OnSalePayOrder",
    array(
        "\Stuls\Helper",
        "HolderPayStatusChanger",
    )
);