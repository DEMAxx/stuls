<?
namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;


class RezanHandler extends Base
{
    public static function getClassTitle()
        {
            return 'Доставка по Рязани';
        }
        
    public static function getClassDescription()
        {
            return 'Доставка по Рязани';
        }
        
    protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment)
        {
            $result = new CalculationResult();
            $price = floatval($this->config["MAIN"]["PRICE"]);
            /*
            $weight = floatval($shipment->getWeight()) / 1000;
        
            $result->setDeliveryPrice(roundEx($price * $weight));*/

            $result->setDeliveryPrice(roundEx($price));
            $result->setPeriodDescription('1 день');
        
            return $result;
        }
        
    protected function getConfigStructure()
        {
            return array(
                "MAIN" => array(
                    "TITLE" => 'Доставка по Рязани обработчик',
                    "DESCRIPTION" => 'Доставка по Рязани обработчик',"ITEMS" => array(
                        "PRICE" => array(
                                    "TYPE" => "NUMBER",
                                    "MIN" => 300,
                                    "NAME" => 'Доставка по Рязани'
                        )
                    ),
                    'PARENT_ID' => 'FROM_SHOP'
                )
            );
        }
        
    public function isCalculatePriceImmediately()
        {
            return true;
        }
        
    public static function whetherAdminExtraServicesShow()
        {
            return true;
        }
}
?>