<?
namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;


class CustomHandler extends Base
{
    public static function getClassTitle()
        {
            return 'Самовывоз';
        }
        
    public static function getClassDescription()
        {
            return 'Самовывоз';
        }
        
    protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment)
        {
            $result = new CalculationResult();
            $price = floatval($this->config["MAIN"]["PRICE"]);
            /*$weight = floatval($shipment->getWeight()) / 1000;*/
        
            $result->setDeliveryPrice(roundEx($price));
            $result->setPeriodDescription('1 день'); 
         
            return $result;
        }
        
    protected function getConfigStructure()
        {
            return array(
                "MAIN" => array(
                    "TITLE" => 'Настройка обработчика',
                    "DESCRIPTION" => 'Настройка обработчика',"ITEMS" => array(
                        "PRICE" => array(
                                    "TYPE" => "NUMBER",
                                    "MIN" => 0,
                                    "NAME" => 'Самовывоз'
                        )
                    )
                )
            );
        }
        
    public function isCalculatePriceImmediately()
        {
            return true;
        }
        
    public static function whetherAdminExtraServicesShow()
        {
            return true;
        }
}
?>