<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentDescription = [
    'NAME' => "LOGIN_BASKET",
    'DESCRIPTION' => "LOGIN_BASKET",
    'ICON' => '/images/news_detail.gif',
    'SORT' => 10,
    'CACHE_PATH' => 'Y',
    'PATH' => [
        'ID' => 'e-store',
    ],
];