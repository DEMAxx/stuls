<?
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;

class OpenSourceOrderComponent extends CBitrixComponent implements Controllerable
{
/**
     * Инициализация обработчика ajax
     */
    public function configureActions()
    {
        return [
            'login'      => [
                'prefilters' => [
                    //new ActionFilter\Authentication(),
                    new ActionFilter\HttpMethod(
                        array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    ),
                    new ActionFilter\Csrf(),
                ],
            ],
        ];
    }

	function executeComponent()
	{
    }
    
    function loginAction($name, $mail)
    {
        $arAnswer = [
            "done"   => true,
            "answer" => json_encode(array("test" => "test")),
        ];        
        
            global $USER;
            if(!$USER->GetLogin()){
                $user = [];
                $user["NAME"] = htmlspecialchars($name);
                $user["LOGIN"] = CUtil::translit($user["NAME"], "ru" , $params);
                $user["LOGIN"] = trim($user["LOGIN"])."_".rand(0, 1000);
                $user["EMAIL"] = $mail;
                $newUser = new CUser;
                $arFields = Array(
                "NAME"              => $user["NAME"],
                "EMAIL"             => $user["EMAIL"],
                "LOGIN"             => $user["LOGIN"],
                "LID"               => "ru",
                "ACTIVE"            => "Y",
                "GROUP_ID"          => array(5),
                "PASSWORD"          => "123456",
                "CONFIRM_PASSWORD"  => "123456",
                );
                
                if($ID = $newUser->Add($arFields)){                    
                    if($USER->Login($user["LOGIN"], "123456", "Y")){
                        $arAnswer["result"] = "success"; 
                    }
                }else{
                    $arAnswer["result"] = "error"; 
                }
                $arAnswer["result"] = json_encode($arAnswer["result"]);
            }
            

            $arAnswer["user"] = json_encode($arFields);
             
            return $arAnswer;
        //} 
    }    
}