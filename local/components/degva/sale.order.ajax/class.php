<?php

use Bitrix\Main;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Controller\PhoneAuth;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;/*
use Bitrix\Main\Web\Json;
use Bitrix\Sale;
use Bitrix\Sale\Delivery;
use Bitrix\Sale\DiscountCouponsManager;
use Bitrix\Sale\Location\GeoIp;
use Bitrix\Sale\Location\LocationTable;
use Bitrix\Sale\Order;
use Bitrix\Sale\Payment;
use Bitrix\Sale\PaySystem;
use Bitrix\Sale\PersonType;
use Bitrix\Sale\Result;
use Bitrix\Sale\Services\Company;
use Bitrix\Sale\Shipment;
*/
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true){
	die();
}
/**
 * @var $APPLICATION CMain
 * @var $USER CUser
 */

class customOrderComponent extends CBitrixComponent
{
	/**
	 * @var \Bitrix\Sale\Order
	 */

	public $order;
	protected $errors = [];

	/** */
	function __construct($component = null)
	{
		parent::__construct($component);
 
		if(!Loader::includeModule('sale')){
			$this->errors[] = 'No sale module';
		};
 
		if(!Loader::includeModule('catalog')){
			$this->errors[] = 'No catalog module';
		};
	}
 
	function onPrepareComponentParams($arParams)
	{
		if (isset($arParams['PERSON_TYPE_ID']) && intval($arParams['PERSON_TYPE_ID']) > 0) {
			$arParams['PERSON_TYPE_ID'] = intval($arParams['PERSON_TYPE_ID']);
		} else {
			if (intval($this->request['payer']['person_type_id']) > 0) {
				$arParams['PERSON_TYPE_ID'] = intval($this->request['payer']['person_type_id']);
			} else {
				$arParams['PERSON_TYPE_ID'] = 1;
			}
		}
 
		return $arParams;
	}
 
	protected function createVirtualOrder()
	{
		global $USER;
 
		try {
			$siteId = \Bitrix\Main\Context::getCurrent()->getSite();
			$basketItems = \Bitrix\Sale\Basket::loadItemsForFUser(
				\CSaleBasket::GetBasketUserID(), 
				$siteId
			)
				->getOrderableItems();
 
			if (count($basketItems) == 0) {
				LocalRedirect('empty.php');
			}
 
			$this->order = \Bitrix\Sale\Order::create($siteId, $USER->GetID());
			$this->order->setPersonTypeId($this->arParams['PERSON_TYPE_ID']);
			$this->order->setBasket($basketItems);
			$this->setOrderProps();
		} catch (\Exception $e) {
			$this->errors[] = $e->getMessage();
		}
	}
	protected function setOrderProps()
	{
		global $USER;
		$arUser = $USER->GetByID(intval($USER->GetID()))
			->Fetch();
 
		if (is_array($arUser)) {
			$fio = $arUser['LAST_NAME'] . ' ' . $arUser['NAME'] . ' ' . $arUser['SECOND_NAME'];
			$fio = trim($fio);
			$arUser['FIO'] = $fio;
		}
		$orders = $this->order->getPropertyCollection();
		pre($this->request);
		foreach ($this->order->getPropertyCollection() as $prop) {
			/** @var \Bitrix\Sale\PropertyValue $prop */
			$value = '';
			switch ($prop->getField('CODE')) {
				case 'FIO':
					$value = $this->request['contact']['family'];
					$value .= ' ' . $this->request['contact']['name'];
					$value .= ' ' . $this->request['contact']['second_name'];
					
					$value = trim($value);
					if (empty($value)) {
						$value = $arUser['FIO'];
					}
					break;
 
				default:
			}
 
			if (empty($value)) {
				foreach ($this->request as $key => $val) {
					if (strtolower($key) == strtolower($prop->getField('CODE'))) {
						$value = $val;
					}
				}
			}
 
			if (empty($value)) {
				$value = $prop->getProperty()['DEFAULT_VALUE'];
			}
 
			if (!empty($value)) {
				$prop->setValue($value);
			}
		}
	}

	function executeComponent()
	{
		$this->createVirtualOrder();
	}
}

Loc::loadMessages(__FILE__);

