<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<div class="contact-page">
	<a href="tel:+78007079724" class="ib-3 phone">
			<p class="text">7 (800) 707 97 24</p>
			<div class="title">Бесплатно по России</div>
	</a><!--
	--><a href="tel:+74912559724" class="ib-3 phone">
			<p class="text">+7 (4912) 55 97 24</p>
			<div class="title">Администратор</div>
	</a><!--
	--><a href="https://yandex.ru/maps/11/ryazan/house/vysokovoltnaya_ulitsa_33/Z0AYcQdgT0UDQFtufXpweH5ibA==/?from=tabbar&ll=39.700604%2C54.619219&sll=39.700604%2C54.619219&source=serp_navig&z=16" target="_blank" class="ib-3 address">
		<p class="text">Рязань, ул. Высоковольтная, д.33</p>
		<div class="title">Адрес магазина</div>
	</a><!-- 
 --><a href="mailto:stulsrzn@stuls.ru" class="ib-3 mail">
 		<p class="text">stulsrzn@stuls.ru</p>
		<div class="title">Почта магазина</div>
	</a><!--
 --><div class="ib-3 clock">
 		<p class="text">с 10:00 до 19:00</p>
		<div class="title">Часы работы</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
        "PATH" => "/include-files/map.php",
        "FULL_TEXT" => "Y"
	)
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>