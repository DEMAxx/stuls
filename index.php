<?
define('MAIN_PAGE','Y');
define('UNIQUE_PAGE','Y');
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty('description', 'STULS Кресла и стулья - лучшая мебель в Рязани и Московской области');
$APPLICATION->SetPageProperty('keywords', 'Кресла, стулья, мебель, офис, стол, столы, стул, диван');
$APPLICATION->SetPageProperty('title', 'STULS Кресла и стулья');
$APPLICATION->SetTitle('STULS Кресла и стулья');
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"main-gallery",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("ID", "CODE", "XML_ID", "NAME", "SORT", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_TEXT", "DETAIL_PICTURE", "DATE_ACTIVE_FROM", "DATE_ACTIVE_TO", ""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "different",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("BANNER_IS_LINK", "LINK", "BACKGROUND_COLOR", ""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
<?$APPLICATION->IncludeComponent(
	'bitrix:catalog.section.list',
	'catalog-section',
	array(
		'ADD_SECTIONS_CHAIN' => 'Y',
		'CACHE_FILTER' => 'N',
		'CACHE_GROUPS' => 'N',
		'CACHE_TIME' => '36000000',
		'CACHE_TYPE' => 'A',
		'COUNT_ELEMENTS' => 'Y',
		'COUNT_ELEMENTS_FILTER' => 'CNT_ACTIVE',
		'CUSTOM_TITLE' => 'Y',
		'FILTER_NAME' => 'sectionsFilter',
		'IBLOCK_ID' => '2',
		'IBLOCK_TYPE' => 'Catalog',
		'SECTION_CODE' => '',
		'SECTION_FIELDS' => array(
			0 => '',
			1 => '',
		),
		'SECTION_ID' => '',
		'SECTION_URL' => '',
		'SECTION_USER_FIELDS' => array(
			0 => '',
			1 => '',
		),
		'SHOW_PARENT_NAME' => 'Y',
		'TOP_DEPTH' => '2',
		'VIEW_MODE' => 'LINE',
		'COMPONENT_TEMPLATE' => 'catalog-section',
		"SHOW_ACTION" => "Y"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	'bitrix:news.list',
	'bestsellers',
	array(
		'ACTIVE_DATE_FORMAT' => 'd.m.Y',
		'ADD_SECTIONS_CHAIN' => 'Y',
		'AJAX_MODE' => 'N',
		'AJAX_OPTION_ADDITIONAL' => '',
		'AJAX_OPTION_HISTORY' => 'N',
		'AJAX_OPTION_JUMP' => 'N',
		'AJAX_OPTION_STYLE' => 'Y',
		'CACHE_FILTER' => 'N',
		'CACHE_GROUPS' => 'N',
		'CACHE_TIME' => '36000000',
		'CACHE_TYPE' => 'A',
		'CHECK_DATES' => 'Y',
		'DETAIL_URL' => '',
		'DISPLAY_BOTTOM_PAGER' => 'Y',
		'DISPLAY_DATE' => 'Y',
		'DISPLAY_NAME' => 'Y',
		'DISPLAY_PICTURE' => 'Y',
		'DISPLAY_PREVIEW_TEXT' => 'Y',
		'DISPLAY_TOP_PAGER' => 'N',
		'FIELD_CODE' => array(
			0 => '',
			1 => '',
		),
		'FILTER_NAME' => 'SHARE',
		'HIDE_LINK_WHEN_NO_DETAIL' => 'N',
		'IBLOCK_ID' => '2',
		'IBLOCK_TYPE' => 'Catalog',
		'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y',
		'INCLUDE_SUBSECTIONS' => 'Y',
		'MESSAGE_404' => '',
		'NEWS_COUNT' => '10',
		'PAGER_BASE_LINK_ENABLE' => 'N',
		'PAGER_DESC_NUMBERING' => 'N',
		'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000',
		'PAGER_SHOW_ALL' => 'N',
		'PAGER_SHOW_ALWAYS' => 'N',
		'PAGER_TEMPLATE' => '.default',
		'PAGER_TITLE' => 'Новости',
		'PARENT_SECTION' => '',
		'PARENT_SECTION_CODE' => '',
		'PREVIEW_TRUNCATE_LEN' => '',
		'PROPERTY_CODE' => array(
			0 => 'SHARE',
			1 => 'SHARE_PRICE',
			2 => '',
		),
		'SET_BROWSER_TITLE' => 'N',
		'SET_LAST_MODIFIED' => 'N',
		'SET_META_DESCRIPTION' => 'N',
		'SET_META_KEYWORDS' => 'N',
		'SET_STATUS_404' => 'N',
		'SET_TITLE' => 'N',
		'SHOW_404' => 'N',
		'SORT_BY1' => 'SHOW_COUNTER',
		'SORT_BY2' => 'SORT',
		'SORT_ORDER1' => 'DESC',
		'SORT_ORDER2' => 'ASC',
		'STRICT_SECTION_CHECK' => 'N',
		'COMPONENT_TEMPLATE' => 'bestsellers'
	),
	false
);?>
<?/*$APPLICATION->IncludeComponent(
	'bitrix:sale.bestsellers',
	'catalog',
	Array(
		'ACTION_VARIABLE' => 'action',
		'ADDITIONAL_PICT_PROP_2' => 'MORE_PHOTO',
		'ADDITIONAL_PICT_PROP_3' => '',
		'ADD_PROPERTIES_TO_BASKET' => 'Y',
		'AJAX_MODE' => 'Y',
		'AJAX_OPTION_ADDITIONAL' => '',
		'AJAX_OPTION_HISTORY' => 'N',
		'AJAX_OPTION_JUMP' => 'N',
		'AJAX_OPTION_STYLE' => 'Y',
		'BASKET_URL' => '/basket/',
		'BY' => 'QUANTITY',
		'CACHE_TIME' => '86400',
		'CACHE_TYPE' => 'A',
		'CART_PROPERTIES_2' => array('',''),
		'CART_PROPERTIES_3' => array('FOOTING','UPHOLSTERY','COLOR',''),
		'CONVERT_CURRENCY' => 'N',
		'DETAIL_URL' => '#SITE_DIR#',
		'DISPLAY_COMPARE' => 'N',
		'FILTER' => array('N'),
		'HIDE_NOT_AVAILABLE' => 'N',
		'LABEL_PROP_2' => '-',
		'LINE_ELEMENT_COUNT' => '3',
		'MESS_BTN_BUY' => 'Купить',
		'MESS_BTN_DETAIL' => 'Подробнее',
		'MESS_BTN_SUBSCRIBE' => 'Подписаться',
		'MESS_NOT_AVAILABLE' => 'Нет в наличии',
		'OFFER_TREE_PROPS_3' => array(),
		'PAGE_ELEMENT_COUNT' => '30',
		'PARTIAL_PRODUCT_PROPERTIES' => 'N',
		'PERIOD' => '180',
		'PRICE_CODE' => array('retail'),
		'PRICE_VAT_INCLUDE' => 'Y',
		'PRODUCT_ID_VARIABLE' => 'id',
		'PRODUCT_PROPS_VARIABLE' => 'prop',
		'PRODUCT_QUANTITY_VARIABLE' => 'quantity',
		'PRODUCT_SUBSCRIPTION' => 'N',
		'PROPERTY_CODE_2' => array('',''),
		'PROPERTY_CODE_3' => array('FOOTING','UPHOLSTERY','COLOR',''),
		'SHOW_DISCOUNT_PERCENT' => 'N',
		'SHOW_IMAGE' => 'Y',
		'SHOW_NAME' => 'Y',
		'SHOW_OLD_PRICE' => 'N',
		'SHOW_PRICE_COUNT' => '1',
		'SHOW_PRODUCTS_2' => 'Y',
		'TEMPLATE_THEME' => 'blue',
		'USE_PRODUCT_QUANTITY' => 'N'
	)
);*/?>
<?$APPLICATION->IncludeComponent(
	'bitrix:main.include',
	'',
	Array(
		'AREA_FILE_SHOW' => 'file',
		'AREA_FILE_SUFFIX' => '',
		'EDIT_TEMPLATE' => '',
		'PATH' => '/include-files/why-us.php'
	)
);?>
<?

global $shareFilter;

$shareFilter[] = [
	"PROPERTY_SHARE_VALUE" => 'Да'
];

$APPLICATION->IncludeComponent(
	'bitrix:news.list',
	'catalog',
	Array(
		'ACTIVE_DATE_FORMAT' => 'd.m.Y',
		'ADD_SECTIONS_CHAIN' => 'Y',
		'AJAX_MODE' => 'N',
		'AJAX_OPTION_ADDITIONAL' => '',
		'AJAX_OPTION_HISTORY' => 'N',
		'AJAX_OPTION_JUMP' => 'N',
		'AJAX_OPTION_STYLE' => 'Y',
		'CACHE_FILTER' => 'N',
		'CACHE_GROUPS' => 'Y',
		'CACHE_TIME' => '36000000',
		'CACHE_TYPE' => 'A',
		'CHECK_DATES' => 'Y',
		'DETAIL_URL' => '',
		'DISPLAY_BOTTOM_PAGER' => 'Y',
		'DISPLAY_DATE' => 'Y',
		'DISPLAY_NAME' => 'Y',
		'DISPLAY_PICTURE' => 'Y',
		'DISPLAY_PREVIEW_TEXT' => 'Y',
		'DISPLAY_TOP_PAGER' => 'N',
		'FIELD_CODE' => array('',''),
		'FILTER_NAME' => 'shareFilter',
		'HIDE_LINK_WHEN_NO_DETAIL' => 'N',
		'IBLOCK_ID' => '2',
		'IBLOCK_TYPE' => 'Catalog',
		'INCLUDE_IBLOCK_INTO_CHAIN' => 'N',
		'INCLUDE_SUBSECTIONS' => 'Y',
		'MESSAGE_404' => '',
		'NEWS_COUNT' => '200',
		'PAGER_BASE_LINK_ENABLE' => 'N',
		'PAGER_DESC_NUMBERING' => 'N',
		'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000',
		'PAGER_SHOW_ALL' => 'N',
		'PAGER_SHOW_ALWAYS' => 'N',
		'PAGER_TEMPLATE' => '.default',
		'PAGER_TITLE' => 'Акции',
		'PARENT_SECTION' => '',
		'PARENT_SECTION_CODE' => '',
		'PREVIEW_TRUNCATE_LEN' => '',
		'PROPERTY_CODE' => array('SHARE','SHARE_PRICE',''),
		'SET_BROWSER_TITLE' => 'N',
		'SET_LAST_MODIFIED' => 'N',
		'SET_META_DESCRIPTION' => 'N',
		'SET_META_KEYWORDS' => 'N',
		'SET_STATUS_404' => 'N',
		'SET_TITLE' => 'N',
		'SHOW_404' => 'N',
		'SORT_BY1' => 'ACTIVE_FROM',
		'SORT_BY2' => 'SORT',
		'SORT_ORDER1' => 'DESC',
		'SORT_ORDER2' => 'ASC',
		'STRICT_SECTION_CHECK' => 'N'
	)
);?>
<?$APPLICATION->IncludeComponent(
	'bitrix:main.include',
	'',
	Array(
		'AREA_FILE_SHOW' => 'file',
		'AREA_FILE_SUFFIX' => '',
		'EDIT_TEMPLATE' => '',
		'PATH' => '/include-files/about.php'
	)
);?>
<?$APPLICATION->IncludeComponent(
	'bitrix:news.list',
	'reviews',
	Array(
		'ACTIVE_DATE_FORMAT' => 'd.m.Y',
		'ADD_SECTIONS_CHAIN' => 'N',
		'AJAX_MODE' => 'N',
		'AJAX_OPTION_ADDITIONAL' => '',
		'AJAX_OPTION_HISTORY' => 'N',
		'AJAX_OPTION_JUMP' => 'N',
		'AJAX_OPTION_STYLE' => 'Y',
		'CACHE_FILTER' => 'N',
		'CACHE_GROUPS' => 'Y',
		'CACHE_TIME' => '36000000',
		'CACHE_TYPE' => 'A',
		'CHECK_DATES' => 'Y',
		'DETAIL_URL' => '',
		'DISPLAY_BOTTOM_PAGER' => 'Y',
		'DISPLAY_DATE' => 'Y',
		'DISPLAY_NAME' => 'Y',
		'DISPLAY_PICTURE' => 'Y',
		'DISPLAY_PREVIEW_TEXT' => 'Y',
		'DISPLAY_TOP_PAGER' => 'N',
		'FIELD_CODE' => array('',''),
		'FILTER_NAME' => '',
		'HIDE_LINK_WHEN_NO_DETAIL' => 'N',
		'IBLOCK_ID' => '5',
		'IBLOCK_TYPE' => 'different',
		'INCLUDE_IBLOCK_INTO_CHAIN' => 'N',
		'INCLUDE_SUBSECTIONS' => 'Y',
		'MESSAGE_404' => '',
		'NEWS_COUNT' => '20',
		'PAGER_BASE_LINK_ENABLE' => 'N',
		'PAGER_DESC_NUMBERING' => 'N',
		'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000',
		'PAGER_SHOW_ALL' => 'N',
		'PAGER_SHOW_ALWAYS' => 'N',
		'PAGER_TEMPLATE' => '.default',
		'PAGER_TITLE' => 'Новости',
		'PARENT_SECTION' => '',
		'PARENT_SECTION_CODE' => '',
		'PREVIEW_TRUNCATE_LEN' => '',
		'PROPERTY_CODE' => array('NAME','DATE','ESTIMATION','REVIEW',''),
		'SET_BROWSER_TITLE' => 'N',
		'SET_LAST_MODIFIED' => 'N',
		'SET_META_DESCRIPTION' => 'N',
		'SET_META_KEYWORDS' => 'N',
		'SET_STATUS_404' => 'N',
		'SET_TITLE' => 'N',
		'SHOW_404' => 'N',
		'SORT_BY1' => 'ACTIVE_FROM',
		'SORT_BY2' => 'SORT',
		'SORT_ORDER1' => 'DESC',
		'SORT_ORDER2' => 'ASC',
		'STRICT_SECTION_CHECK' => 'N'
	)
);?><?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>