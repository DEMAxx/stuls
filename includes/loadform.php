<?
header('Access-Control-Allow-Origin: *');
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php'); 
$iblockId = false;
$iblockId = ((int)$_REQUEST['id'])?(int)$_REQUEST['id']:(int)$_REQUEST['BASE_IBLOCK_ID'];
if(!empty($_REQUEST['id'])){
	$iblockTitle = $_REQUEST['title'];
}
/*if($iblockId == 1){*/
$customTitle = 'Оставить отзыв';
$property_list = array('NAME', '4', '6', '30', '31', '26', '27','28','29');
$property_reqlist = array('NAME', '4', '6', '30', '31', '26', '27','28','29'); 
$IBLOCK_ID = 5;
$title = 'Оставить отзыв';
/*}elseif($iblockId == 2){
	$customTitle = 'Напиши нам';
	$property_list = array('NAME', '13', '14', '15',);
	$property_reqlist = array('NAME', '13', '14', '15',);
	$IBLOCK_ID = 5;
	$title = 'Напиши нам';
} */
$APPLICATION->IncludeComponent(
	"degva:iblock.element.add.form", 
	"review",
	array(
		"EMAIL_TO" => "sdeminv@yandex.ru",
		"CUSTOM_NAME" => $title,
		"SEF_MODE" => "N",
		"IBLOCK_TYPE" => "forms",
		"IBLOCK_ID" => $IBLOCK_ID,
		"BASE_IBLOCK_ID" => $iblockId,
		"PROPERTY_CODES" => $property_list,
		"PROPERTY_CODES_REQUIRED" => $property_reqlist,
		"GROUPS" => array(
			0 => "1",
			1 => "2",
			2 => "3",
			3 => "4",
		),
		"STATUS_NEW" => "N",
		"STATUS" => "ADD",
		"LIST_URL" => "",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"MAX_USER_ENTRIES" => "100000",
		"MAX_LEVELS" => "100000",
		"LEVEL_LAST" => "Y",
		"USE_CAPTCHA" => "N",
		"USER_MESSAGE_EDIT" => "",
		"USER_MESSAGE_ADD" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"RESIZE_IMAGES" => "N",
		"USE_AGREEMENT" => "N",
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"CUSTOM_TITLE_NAME" => $customTitle,
		"CUSTOM_TITLE_TITLE" => $iblockTitle,
		"CUSTOM_TITLE_TAGS" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_SHADOW" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);
?>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
?>