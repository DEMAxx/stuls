<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="why-us">
    <div class="center1200">
        <h3 class="h1">Почему покупатели выбирают&nbsp;нас?</h3>
    </div>
    <div class="ib-2-wrapper">
        <div class="ib-2 ib-4">
            <div class="img-box">
                <img src="/images/svg/chair.svg" alt="Широкий ассортимент продукции" title="Широкий ассортимент продукции" class="img"/>
            </div>
            <p class="text">Широкий ассортимент продукции</p>
        </div>
        <div class="ib-2 ib-4">
            <div class="img-box">
                <img src="/images/svg/wallet.svg" alt="Всегда выгодные цены" title="Всегда выгодные цены" class="img"/>
            </div>
            <p class="text">Всегда выгодные<br/>цены</p>
        </div>
        <div class="ib-2 ib-4">
            <div class="img-box">
                <img src="/images/svg/truck.svg" alt="Удобная и быстрая доставка" title="Удобная и быстрая доставка" class="img"/>
            </div>
            <p class="text">Удобная и быстрая<br/>доставка</p>
        </div>
        <div class="ib-2 ib-4">
            <div class="img-box">
                <img src="/images/svg/sale.svg" alt="Распродажи и акции" title="Распродажи и акции" class="img"/>
            </div>
            <p class="text">Распродажи<br/>и акции</p>
        </div>
    </div>

</div>