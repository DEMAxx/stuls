<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=<?=\Bitrix\Main\Config\Option::get('fileman', 'yandex_map_api_key')?>71f76e7e-6710-4010-8312-ff72b9a92264" type="text/javascript"></script>
<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/map.js');?>
<div class="map" id="map">
</div>
<div class="center1200 contact-page contact-bottom">
    <div class="vertical-line">
    </div>
    <div class="right-block">
        <p>
            ИНН: 623412560366
        </p>
        <p>
            Банк получателя: Точка ПАО Банка «ФК Открытие» г. Москва
        </p>
        <p>
            Расчетный счет: 40802810502500080464
        </p>
        <p>
            Корреспондентский счет: 30101810845250000999
        </p>
        <p>
            БИК: 044525999
        </p>
        <p>
            ОГРН: 320623400000410
        </p>
    </div>
    <p class="title">
        НАИМЕНОВАНИЕ КОМПАНИИ
    </p>
    <p>
        ИП Машинская Екатерина Вячеславовна
    </p>
    <p>
        <br>
    </p>
    <p class="title">
        РЕГИСТРАЦИОННАЯ ИНФОРМАЦИЯ
    </p>
    <p>
        Юридический адрес: г. Рязань, ул. Высоковольтная, д.18, кв. 82<br>
    </p>
    <p>
        Фактический адрес: г. Рязань, ул. Высоковольтная, д.33 ТЦ "Квазар" 2 этаж
    </p>
    <br>
    <div class="clear test">
    </div>
</div>
<br>