<?

define("UNIQUE_PAGE", "Y");

// подключение файла обработки адресов urlrewrite.php
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

// установка HTTP статуса 404
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");
// скрывает боковую панель на странице
define("HIDE_SIDEBAR", true);

// подключение header.php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Stuls");
?>
    <div class="center1200 center-block">
        <div class="info-page-wrapper">
            <div class="info-page-image">
                <img class="img" src="/images/svg/sad.svg" alt="">
            </div>
            <h1 class="h1">ОШИБКА 404</h1>
            <p>Страница не найдена или была удалена.</p>
            <br/>
            <a class="button" href="/">Вернуться на главную</a>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>