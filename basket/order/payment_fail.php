<?define("UNIQUE_PAGE","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
echo "
<style>
@media only screen and (min-width: 960px){
	.header .top-line{ 
		height: 60px;
	}
	.header .top-line:before{
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
	}
}
</style>
";
?>
<div class="center1200 center-block">
    <div class="info-page-wrapper">
        <div class="info-page-image">
            <img class="img" src="/images/svg/sad.svg" alt="">
        </div>
        <h1 class="h1">Платеж отклонен</h1>
        <p>Ваш заказ не оплачен и не будет обработан. Попробуйте повторить заказ.</p>
        <br/>
        <a class="button" href="/">Вернуться на главную</a>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>