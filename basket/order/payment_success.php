<?define("UNIQUE_PAGE","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
echo "
<style>
@media only screen and (min-width: 960px){
	.header .top-line{ 
		height: 60px;
	}
	.header .top-line:before{
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
	}
}
</style>
";
preg_match('/(success)/', $_SERVER["HTTP_REFERER"], $matches, PREG_OFFSET_CAPTURE);
if(!$matches){
    LocalRedirect("payment_fail.php");
}
?>
<div class="center1200 center-block">
    <div class="info-page-wrapper">
        <div class="info-page-image">
            <img class="img" src="/images/svg/success.svg" alt="">
        </div>
        <h1 class="h1">Заказ оплачен</h1>
        <p>Ваш заказ успешно оформлен и оплачен.</p>
        <br/>
        <a class="button" href="/">Вернуться на главную</a>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>